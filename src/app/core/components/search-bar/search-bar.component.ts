import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormControl } from '@angular/forms';
import { setConfigFuseConfigService } from '../../services/config.service';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import { startWith } from 'rxjs/operators/startWith';
import { map } from 'rxjs/operators/map';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { editCustomerComponent } from '../../../main/modals/editCustomer/editCustomer.component';
import { addCustomerComponent } from '../../../main/modals/addCustomer/addCustomer.component';
import { AuthServiceService } from '../../../../services/auth-service.service';
import { Router } from '@angular/router';


@Component({
    selector: 'fuse-search-bar',
    templateUrl: './search-bar.component.html',
    styleUrls: ['./search-bar.component.scss']
})
export class FuseSearchBarComponent {

    collapsed: boolean;
    toolbarColor: string;
    @Output() onInput: EventEmitter<any> = new EventEmitter();
    onSettingsChanged: Subscription;
    // fuseSearch: string;

    constructor(
        private matDialog: MatDialog,
        private fuseConfig: setConfigFuseConfigService,
        private authSrvc: AuthServiceService,
        private router: Router,

    ) {
        this.collapsed = true;
        this.onSettingsChanged =
            this.fuseConfig.onSettingsChanged
                .subscribe(
                    (newSettings) => {
                        this.toolbarColor = newSettings.colorClasses.toolbar;
                    }
                );
    }


    fuseSearch() {
        // console.log("evnt", ev);
        this.options = [];
        // document.getElementById('fuse-search-bar').innerHTML='';
    }

    editCustomerComponent(option) {
        this.collapse();
        this.matDialog.open(editCustomerComponent, {
            panelClass: 'adddialog',
            data: { id: option.id }
        });

    }
    addCustomerComponent() {
        this.matDialog.open(addCustomerComponent, {
            panelClass: 'adddialog',
        });
    }

    ngOnInitw() {

    }

    collapse() {
        this.collapsed = true;
    }

    expand() {
        this.collapsed = false;
        // this.router.navigate['toolbar'];
    }

    search(event) {
        const value = event.target.value;

        this.onInput.emit(value);

        let customerNames = {
            name: value,
            company_id: JSON.parse(localStorage.getItem('company_id'))
        }
        this.authSrvc.matchCustomerNames(customerNames).subscribe((res: any) => {
            this.options = res.response;
        });

    }






    myControl: FormControl = new FormControl();

    options = [
        // 'Genevieve Calley',  
        // 'Lavelle Potter',  
        // 'Bess Blansett',
        // 'Chrissy Unsworth', 
        // 'Mozella Llanos', 
        // 'Norman Saavedra',  
        // 'Nichelle Scheu', 
        // 'Argelia Parkman',  
        // 'Tobi Turnquist',  
        // 'Rudolph Cobian',  
        // 'Lanny Koll',  
        // 'Beatris Looney',  
        // 'Graig Petzold',  
        // 'Zandra Bolinger',  
        // 'Romeo Hancock',  
        // 'Sheridan Violet',  
        // 'Virginia Mccay',  
        // 'Selena Lain',  
        // 'Le Mckiernan',  
        // 'Rosalind Longacre'  
    ];

    filteredOptions: Observable<string[]>;

    // ngOnInit() {
    //     this.filteredOptions = this.myControl.valueChanges
    //         .pipe(
    //             startWith(''),
    //             map(val => this.filter(val))
    //         );
    // }

    // filter(val: string): string[] {
    //     return this.options.filter(option =>
    //         option.toLowerCase().indexOf(val.toLowerCase()) === 0);
    // }

}
