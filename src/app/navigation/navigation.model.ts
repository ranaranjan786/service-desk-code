import { FuseNavigationModelInterface } from '../core/components/navigation/navigation.model';

export class FuseNavigationModel implements FuseNavigationModelInterface
{
    public model: any[];

    constructor()
    {
        this.model = [
            {
                'id'      : 'applications',
                'type'    : 'group',
                'children': [
          
                     {
                        'title'    : 'Home',
                        'type'     : 'item',
                        'icon'     : 'home',
                        'url'      : '/home'
                    },
                     {
                        'title'    : 'Jobs',
                        'type'     : 'collapse',
                        'icon'     : 'home',
                        'children' : [
                            {
                                'title': 'Jobs',
                                'type' : 'item',
                                'url'  : '/my-job'
                            },
                            {
                                'title': 'Estimates',
                                'type' : 'item',
                                'url'  : '/estimates'
                            },
                            {
                                'title': 'Invoice',
                                'type' : 'item',
                                'url'  : '/invoice'
                            },
                            {
                                'title': 'Payments',
                                'type' : 'item',
                                'url'  : '/payments'
                            },
                            {
                                'title': 'Contracts',
                                'type' : 'item',
                                'url'  : '/contracts'
                            },
                            
                        ]
                    },

                    {
                        'title'    : 'Reports',
                        'type'     : 'item',
                        'icon'     : 'pie_chart',
                        'url'      : '/reports'
                    },

                    {
                        'title' : 'Customers',
                        'type' : 'item',
                        'icon' : 'people',
                        'url' : '/customers'
                        },
                    
                     {
                        'title'    : 'Calendar',
                        'type'     : 'item',
                        'icon'     : 'today',
                        'url'      : '/calendar'
                    },

                    
                ]
            }
        ];
    }
}
