import { InMemoryDbService } from 'angular-in-memory-web-api';


import { ContactsDb } from './customers';
import { TodoFakeDb } from './todo';

export class DbService implements InMemoryDbService
{
    createDb()
    {
        return {
            
            // customers
            'customers-customers': ContactsDb.customers,
            'customers-user'    : ContactsDb.user,
            
            // Todo
            'todo-todos'  : TodoFakeDb.todos,
            'todo-filters': TodoFakeDb.filters,
            'todo-tags'   : TodoFakeDb.tags,
            
        };
    }
}
