import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { setConfigFuseConfigService } from '../../../../core/services/config.service';
import { fuseAnimations } from '../../../../core/animations';
import { AuthServiceService } from '../../../../../services/auth-service.service';
import { Router } from '@angular/router';
import { Config } from '../../../../../config';

@Component({
    selector: 'fuse-forgot-password',
    templateUrl: './forgot-password.component.html',
    styleUrls: ['./forgot-password.component.scss'],
    animations: fuseAnimations
})
export class FuseForgotPasswordComponent implements OnInit {
    forgotPasswordForm: FormGroup;
    forgotPasswordFormErrors: any;

    constructor(
        private fuseConfig: setConfigFuseConfigService,
        private formBuilder: FormBuilder,
        private router: Router,
        private authService: AuthServiceService,
        private config: Config
    ) {
        this.fuseConfig.setSettings({
            layout: {
                navigation: 'none',
                toolbar: 'none',
                footer: 'none'
            }
        });

        this.forgotPasswordFormErrors = {
            email: {}
        };
    }

    ngOnInit() {
        this.forgotPasswordForm = this.formBuilder.group({
            email: ['', [Validators.required, Validators.email]]
        });

        this.forgotPasswordForm.valueChanges.subscribe(() => {
            this.onForgotPasswordFormValuesChanged();
        });
    }

    onForgotPasswordFormValuesChanged() {
        for (const field in this.forgotPasswordFormErrors) {
            if (!this.forgotPasswordFormErrors.hasOwnProperty(field)) {
                continue;
            }

            // Clear previous errors
            this.forgotPasswordFormErrors[field] = {};

            // Get the control
            const control = this.forgotPasswordForm.get(field);

            if (control && control.dirty && !control.valid) {
                this.forgotPasswordFormErrors[field] = control.errors;
            }
        }
    }

    sendEmail(email) {

        let passwordObj = {
            email: email,
            company_name: window.location.origin
        }; 

        this.authService.forgetPassword(passwordObj)
            .subscribe((data: any) => {
                // console.log(data)
                if (data.response == 'email sent successfully') {
                    this.config.openSnackBar('Email Has Been Sent!');
                    this.router.navigate(['/']);
                }
                else {
                    this.config.openSnackBar('Invalid Email Address');
                }
            },
                err => {
                    this.config.openSnackBar('Error Occured');
                })
    }

}
