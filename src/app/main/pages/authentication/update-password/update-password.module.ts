import { NgModule } from '@angular/core';
import { SharedModule } from '../../../../core/modules/shared.module';
import { RouterModule } from '@angular/router';

import { UpdatePasswordComponent } from './update-password.component';

const routes = [
    {
        path     : 'reset-password',
        component: UpdatePasswordComponent
    }
];

@NgModule({
    declarations: [
        UpdatePasswordComponent
    ],
    imports     : [
        SharedModule,
        RouterModule.forChild(routes)
    ]
})

export class UpdatePasswordModule
{

}
