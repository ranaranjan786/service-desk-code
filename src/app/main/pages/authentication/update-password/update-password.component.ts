import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { setConfigFuseConfigService } from '../../../../core/services/config.service';
import { fuseAnimations } from '../../../../core/animations';
import { AuthServiceService } from '../../../../../services/auth-service.service';
import { Config } from '../../../../../config';
import { ActivatedRoute, Params, Router } from '@angular/router'; 

@Component({
    selector: 'fuse-forgot-password',
    templateUrl: './update-password.component.html',
    styleUrls: ['./update-password.component.scss'],
    animations: fuseAnimations
})
export class UpdatePasswordComponent implements OnInit {
    forgotPasswordFormErrors: any;
    password = {
        new: "",
        confirm: ""
    }
    token: string = '';
    constructor(
        private fuseConfig: setConfigFuseConfigService,
        private formBuilder: FormBuilder,
        private authService: AuthServiceService,
        private config: Config,
        private activatedRoute: ActivatedRoute,
        private router: Router

    ) {
        this.fuseConfig.setSettings({
            layout: {
                navigation: 'none',
                toolbar: 'none',
                footer: 'none'
            }
        });

        this.forgotPasswordFormErrors = {
            email: {}
        };
    }

    ngOnInit() {

        this.activatedRoute.queryParams.subscribe((params: Params) => {
            this.token = params['token'];
            //api call
            this.authService.validateToken({ token: this.token })
                .subscribe((data: any) => {
                    if (data.response != "Valid Token") {
                        this.config.openSnackBar('Invalid Link! Try Again.');
                        this.router.navigate['/'];
                    }

                }, err => {
                    this.config.openSnackBar('Invalid Link! Try Again.');
                    this.router.navigate['/'];
                });
        });

    }



    checkPassword(password) {

        // this.activatedRoute.queryParams.subscribe((params: Params) => {
        //     this.token = params['token'];

            // let params = (new URL(String(document.location))).searchParams;
            
            // this.token = params.get("token");
            this.token=window.location.href.split('token=')[1];
            debugger
            //api call
            this.authService.validateToken({ token: this.token })
                .subscribe((data: any) => {
                    
                    if (data.response != "Valid Token") {
                        this.config.openSnackBar('Invalid Link! Try Again.');
                        // this.router.navigate(['/']);
                    }
                    else {
                        if (password.new == password.confirm) {
                            let passwordObj = {
                                new_password: password.new,
                                token: this.token
                            }
                            this.authService.resetPassword(passwordObj)
                                .subscribe((data: any) => {

                                    if (data.message != "error") {
                                        this.config.openSnackBar('Password Updated Successfully!')
                                        this.router.navigate(['/']);
                                    }
                                    else {
                                        this.config.openSnackBar('Invalid Reset Link!')
                                    }
                                });
                        }
                        else {
                            this.config.openSnackBar('Passwords Did Not Match!')
                        }
                    }
                }, err => {
                    this.config.openSnackBar('Invalid Link! Try Again.');
                    this.router.navigate(['/']);
                });
        // });


    }
}
