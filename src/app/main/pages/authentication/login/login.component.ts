import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { setConfigFuseConfigService } from '../../../../core/services/config.service';
import { fuseAnimations } from '../../../../core/animations';
import { AuthServiceService } from '../../../../../services/auth-service.service';;
import { Router } from '@angular/router';
import { Config } from '../../../../../config';
import { GlobalService } from '../../../../../services/global-service';

@Component({
    selector: 'fuse-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    animations: fuseAnimations
})
export class FuseLoginComponent implements OnInit {
    loginForm: FormGroup;
    loginFormErrors: any;
    user: any = {};
    errorResponse = '';
    showLogin: boolean = false;

    constructor(
        private fuseConfig: setConfigFuseConfigService,
        private formBuilder: FormBuilder,
        private authService: AuthServiceService,
        private router: Router,
        private config: Config,
        private globalService: GlobalService
    ) {
        this.fuseConfig.setSettings({
            layout: {
                navigation: 'none',
                toolbar: 'none',
                footer: 'none'
            }
        });

        this.loginFormErrors = {
            email: {},
            password: {}
        };


        setTimeout(() => {
            this.showLogin = true;
        }, 2000);
    }




    ngOnInit() {

        // this.checkUrlComapnyName();

        this.checkUrlComapnyName();

        this.loginForm = this.formBuilder.group({
            email: ['', [Validators.required, Validators.email]],
            password: ['', Validators.required]
        });
        this.loginForm.valueChanges.subscribe(() => {
            this.onLoginFormValuesChanged();
        });



    }




    checkUrlComapnyName() {
        let completeUrl = this.globalService.getCompanyName();
        let indexOfFirstDot = completeUrl.indexOf('.');
        let indexOfLastDot = completeUrl.lastIndexOf('.');
        if (indexOfFirstDot == indexOfLastDot) {
            this.router.navigate(['']);
        }
        else {
            let obj = {
                company_name: window.location.href
            }
            this.authService.checkCompany(obj)
                .subscribe((data: any) => {

                    if (data.response == "success") {

                        var token = localStorage.getItem('token');
                        if (token != null) {
                            var tokenVal = {
                                tokenValue: token
                            }
                            this.authService.loginCheck(tokenVal)
                                .subscribe(
                                    (data: any) => {

                                        if (data.result != "error") {
                                            this.router.navigate(['dashboard']); //check where to redirect!!
                                            this.showLogin = false;
                                        }
                                        else {
                                            this.router.navigate(['login']);
                                            localStorage.setItem('token', '');
                                            this.showLogin = true;
                                        }
                                        //console.log("login response",data);
                                        // console.log('token', data['response']);
                                    },
                                    error => {
                                        console.log(error);
                                    }
                                );
                        } else {
                            this.router.navigate(['/']);
                        }
                    }
                    else {
                        // alert("Error 404");
                        this.router.navigate(['error']);
                    }
                },
                    (err: any) => {
                        this.router.navigate(['error']);
                    });
        }
        // let tempUrl = completeUrl.split('.')[0];
    }

    onLoginFormValuesChanged() {
        for (const field in this.loginFormErrors) {
            if (!this.loginFormErrors.hasOwnProperty(field)) {
                continue;
            }

            // Clear previous errors
            this.loginFormErrors[field] = {};

            // Get the control
            const control = this.loginForm.get(field);

            if (control && control.dirty && !control.valid) {
                this.loginFormErrors[field] = control.errors;
            }
        }
    }

    loginFn(user) {
        this.user.company_name = this.config.companyUrl;
        localStorage.setItem('company_name', this.user.company_name);
        this.authService.getLogin(user)
            .subscribe(
                (data: any) => {
                    // console.log("login response",data);
                    // console.log('token', data['response']);
                    if (data['response'] == 'success') {
                        var token = data['token'];
                        localStorage.setItem('token', token);
                        localStorage.setItem('user_name', data['name']);
                        localStorage.setItem('company_id', data['company_id']);
                        localStorage.setItem('user_id', data['user_id']);
                        this.router.navigate(['dashboard']);
                        // var url = this.router.url;


                    } else {
                        this.errorResponse = data.response;
                        // this.router.navigate(['']);

                    }
                },
                error => {
                    console.log(error);
                    this.config.openSnackBar('Error occured!')
                }
            );
    }


}
