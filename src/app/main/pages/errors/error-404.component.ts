

import { Component } from '@angular/core';
import { setConfigFuseConfigService } from '../../../core/services/config.service';

@Component({
    selector: 'error-404',
    templateUrl: './error-404.component.html',
    styleUrls: ['./error-404.component.scss']
})
export class FuseError404Component {
    routeToLogin = "/";
    constructor(
        private fuseConfig: setConfigFuseConfigService
    ) {
        this.fuseConfig.setSettings({
            layout: {
                navigation: 'none',
                toolbar: 'none',
                footer: 'none'
            }
        });
    }

    ngOnInit() {
    }
}

