import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MatIconModule } from '@angular/material';

import { SharedModule } from '../../../core/modules/shared.module';

import { FuseError404Component } from './error-404.component';

const routes = [
    {
        path     : 'error',
        component: FuseError404Component
    }
];

@NgModule({
    declarations: [
        FuseError404Component
    ],
    imports     : [
        RouterModule.forChild(routes),

        MatIconModule,

        SharedModule
    ]
})
export class Error404Module
{
}
