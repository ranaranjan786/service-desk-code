import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ReportsComponent } from './reports.component';

const routes = [
    {
        path     : 'reports',
        component: ReportsComponent
    }
];

@NgModule({
    declarations: [
        ReportsComponent
    ],
    imports     : [
        RouterModule.forChild(routes)
    ]
})

export class ReportsModule
{

}
