import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { PaymentsComponent } from './payments.component';

const routes = [
    {
        path     : 'payments',
        component: PaymentsComponent
    }
];

@NgModule({
    declarations: [
        PaymentsComponent
    ],
    imports     : [
        RouterModule.forChild(routes)
    ]
})

export class PaymentsModule
{

}
