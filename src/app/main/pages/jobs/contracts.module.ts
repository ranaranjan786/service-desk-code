import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ContractsComponent } from './contracts.component';

const routes = [
    {
        path     : 'contracts',
        component: ContractsComponent
    }
];

@NgModule({
    declarations: [
        ContractsComponent
    ],
    imports     : [
        RouterModule.forChild(routes)
    ]
})

export class ContractsModule
{

}
