import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { fuseAnimations } from '../../../core/animations';

@Component({
    templateUrl: './invoice.component.html',
    styleUrls  : ['./invoice.component.scss'],
    animations : fuseAnimations
})
export class InvoiceComponent
{

}
