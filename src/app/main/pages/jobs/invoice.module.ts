import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { InvoiceComponent } from './invoice.component';

const routes = [
    {
        path     : 'invoice',
        component: InvoiceComponent
    }
];

@NgModule({
    declarations: [
        InvoiceComponent
    ],
    imports     : [
        RouterModule.forChild(routes)
    ]
})

export class InvoiceModule
{

}
