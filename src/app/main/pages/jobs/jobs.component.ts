import { Component, OnInit, NgZone } from '@angular/core';
import { FormControl } from '@angular/forms';
import { fuseAnimations } from '../../../core/animations';
import { MatTableDataSource } from '@angular/material';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { addCustomerComponent } from '../../modals/addCustomer/addCustomer.component';
import { editCustomerComponent } from '../../modals/editCustomer/editCustomer.component';
import { addScheduleComponent } from '../../modals/addSchedule/addSchedule.component';  
import { editScheduleComponent } from '../../modals/editSchedule/editSchedule.component';  
import { addServiceComponent } from '../../modals/addService/addService.component';
import { editServiceComponent } from '../../modals/editService/editService.component';
import { AuthServiceService } from '../../../../services/auth-service.service';
import { Router, ActivatedRoute } from '@angular/router';



// import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
// import { Ng2FileInputService, Ng2FileInputAction } from 'ng2-file-input';
import { isBoolean } from 'util';
import { Config } from '../../../../config';
import { GlobalService } from '../../../../services/global-service';

@Component({
  templateUrl: './jobs.component.html',
  styleUrls: ['./jobs.component.scss'],
  animations: fuseAnimations
})

export class JobsComponent {
  tabObj: string;
  radioObj: string;
  public actionLog: string = "";

  names: any = [];
  selectednameid = '';
  result: any = [];
  tickedId: number;
  // disableEditSrvc:boolean=false;

  displayedColumns = ['approved', 'quantity', 'name', 'description', 'cost', 'rate', 'amount', 'tax', 'delete'];
  dataSource = new MatTableDataSource(ELEMENT_DATA);

  paymentColumns = ['paymentreceived', 'invoice', 'paymentmethod', 'amount', 'ref', 'memo'];
  paymentdetails = new MatTableDataSource(PAYMENT_DATA);

  pendingschColumns = ['date', 'time', 'assignee', 'job'];
  pendingappschdule = new MatTableDataSource(PENDINGAPP_DATA);

  invoicedataColumns = ['date', 'period', 'sentto', 'payment', 'invoice'];
  invoicedata = new MatTableDataSource(INVOICESCH_DATA);

  date = new FormControl(new Date());
  serializedDate = new FormControl((new Date()).toISOString());

  selectedValue: string;
  clientService: any = [];
  tmpClientService: any[];
  clientServiceData: number;
  selectedServiceId = '';
  scheduledDate = new FormControl(new Date());
  scheduledDateValue = new FormControl((new Date()).toISOString()).value

  service: any = {
    id: '',
    customer_id: '',
    company_id: '',
    address: '',
    phone: '',
    email: '',
    state: '',
    city: '',
    address1: '',
    altphone: '',
    first_name: '',
    last_name: '',
    address2: '',
    zip: '',
    saletax: undefined
  };
  username = [];
  statusValue: number = 1;
  categoryValue: number = 1;
  shortDescription: string;
  description: string;
  // scheduledDate: Date = undefined;
  assignedTo: number;

  constructor(
    private matDialog: MatDialog,
    private authService: AuthServiceService,
    private router: Router,
    public route: ActivatedRoute,
    private config: Config,
    private globalService: GlobalService

  ) {

    // this.route.params.subscribe(val => {
    //   // this.ngOnInit();
    //   // location.reload();
    //   this.initFunction(true);
    //   this.initCustFunction();
    //   this.getUsername();


    // });
    this.tabObj = "openD";
    this.radioObj = "cash";

  }

  ngOnInit() {
    this.initCustFunction();

    this.initFunction(true);
    // this.getUsername();
    if (String(this.router.url) != "/jobs") {
      localStorage.setItem('ticketId', null);
      localStorage.setItem('jobsData', null);
    }



    // this.route.queryParams
    //   .filter(params => params.result)
    //   .subscribe(params => {
    //     console.log(params); // {order: "popular"}

    //     this.service = params.result;
    //     console.log(this.service); // popular
    //   });



  }

  ngOnDestroy() {

    if (this.router.url != "/jobs") {
      localStorage.setItem('ticketId', null);
      localStorage.setItem('jobsData', null);
    }
  }


  getUsername() {
    let company_id = {
      cmp_id: JSON.parse(localStorage.getItem('company_id'))
    }
    this.authService.getAllUserData(company_id)
      .subscribe(
        data => {
          this.username = data['response'];
          this.assignedTo = this.username[0].id;
        },
        error => {
          // console.log(error);
        }
      );
  }



  //here code for customer 
  initCustFunction() {
    var token = localStorage.getItem('token');
    if (token != null) {
      var tokenVal = {
        tokenValue: token
      }
      /*
        To validate user
      */
      this.authService.loginCheck(tokenVal)
        .subscribe(
          data => {
            // console.log("login response",data);
            if (data['result'] == 'error') {

              localStorage.setItem('token', '');
              this.router.navigate(['/']);
            } else {
              //check if '/jobs' is entered from url or real routing

              this.globalService.currentCustomerData  //from observable
                .subscribe((data: any) => {
                  if (data.id) {
                    this.router.navigate(['/jobs']);
                    this.getUsername();
                    //if successful then get data
                    let user = {
                      user_id: JSON.parse(localStorage.getItem('user_id')),
                      company_id: JSON.parse(localStorage.getItem('company_id'))
                    }
                    this.authService.getAllCustomer(user)
                      .subscribe(
                        data => {
                          this.names = data['response'];
                          // console.log("names",data)
                          localStorage.setItem('names', JSON.stringify(this.names));
                        },
                        error => {
                          // console.log(error);
                        }
                      );
                  }
                  else { //to fill up jobs page on reload
                    if (JSON.parse(localStorage.getItem('ticketId'))) {


                      this.router.navigate(['/jobs']);
                      this.tickedId = JSON.parse(localStorage.getItem('ticketId'));
                      let jobsData = JSON.parse(localStorage.getItem('jobsData'));
                      console.log("jobsData", JSON.parse(localStorage.getItem('jobsData')));
                      console.log(' names: ', JSON.parse(localStorage.getItem('names')));
                      if (jobsData) {
                        this.selectednameid = jobsData.selectednameid;
                        this.service = jobsData.service;
                        this.result = jobsData.result;
                        this.tmpClientService = jobsData.tmpClientService;
                        this.clientServiceData = jobsData.clientServiceData;
                        this.names = JSON.parse(localStorage.getItem('names'));
                        if (jobsData.result.address2 == null || jobsData.result.address2 == '') {
                          jobsData.result.address2 = '';
                          jobsData.service.address2 = '';
                        }
                        this.result.address = jobsData.result.address1 + ' ' + jobsData.result.address2;
                        this.service.address = jobsData.service.address1 + ' ' + jobsData.service.address2;
                      }

                    }
                    else {
                      this.router.navigate(['/dashboard']);  //to be checked where to redirect
                    }
                  }
                });
            }

          },
          error => {
            //console.log(error);
          }
        );
    } else {
      this.router.navigate(['/']);
    }
    // var user = {
    //   user_id: 1
    // }


  }


  //to get values according to customer name
  onChange(newValue) {
    var customerid = {
      id: newValue,
      company_id: JSON.parse(localStorage.getItem('company_id'))
    }

    localStorage.setItem("customer_id", newValue);
    let temp = this.clientService.filter((arr) => arr.customer_id == newValue);
    //this.clientService = temp;
    this.tmpClientService = temp;
    // console.log("this.clientService",this.clientService);

    this.authService.getClientdata(customerid)
      .subscribe(
        data => {
          // console.log("getClientdata data", data);
          //  this.result= data['response'][0];
          let id = data['response'][0]['id'];
          this.getServiceDetailsFn(id);
          this.clientServiceData = id;
        },
        error => {
          //console.log(error);
        }
      );
    //console.log('new change value',newValue);
    this.authService.getCustomerdata(customerid)
      .subscribe(
        data => {
          this.result = data['response'][0];
          this.result.address = data['response'][0]['address1'] + " " + data['response'][0]['address2'];
          //this.getServiceDetailsFn(id);
        },
        error => {
          //console.log(error);
        }
      );

    // ... do other stuff here ...
  }
  //here code to default value
  initFunction(flag: boolean) {
    var token = localStorage.getItem('token');
    this.clientService = [];
    this.tmpClientService = [];
    if (token != null) {


      var user = {
        user_id: JSON.parse(localStorage.getItem('user_id')),
        company_id: JSON.parse(localStorage.getItem('company_id'))
      }
      this.authService.getClientService(user)
        .subscribe(
          (data: any) => {
            this.clientService = [];
            for (let i = 0; i < data.response.length; i++) {
              let name = data.response[i].first_name + ' ' + data.response[i].last_name;
              let arrayText = {
                id: data.response[i].id,
                customer_id: data.response[i].customer_id,
                name: name,
              }
              this.clientService.push(arrayText);
            }
            // console.log("clntsrvc",this.clientService);
            // console.log("dfd",localStorage.getItem("customer_id"))


            // if (localStorage.getItem("customer_id")) {
            if (this.selectednameid) {
              // let customer_id = localStorage.getItem("customer_id");
              let temp = this.clientService.filter((arr) => arr.customer_id == this.selectednameid);
              //this.clientService = temp;
              this.tmpClientService = temp;
            } else {
              this.tmpClientService = this.clientService;
            }



            if (flag) {   // if controlFlow comes from jobComponent Init function

              this.globalService.currentCustomerData  //from observable
                .subscribe((data: any) => { 

                  this.result = data;
                  if (this.result.address2 == null) {
                    this.result.address2 = ''
                  }
                  this.result.address = this.result.address1 + ' ' + this.result.address2;
                  
                  if(data.id){
                    this.result.customerId=data.id;
                  }
                  this.selectednameid = this.result.customerId;

                  let temp = this.clientService.filter((arr) => arr.customer_id == this.selectednameid);
                  this.tmpClientService = temp;
                  if (this.tmpClientService[0]) {
                    this.clientServiceData = this.tmpClientService[0].id;


                    this.getServiceDetailsFn(this.clientServiceData);

                    //createWorkOrder
                    // debugger
                    let jobs_data = {
                      client_id: this.tmpClientService[0].id,
                      customer_id: this.result.customerId,
                      user_id: JSON.parse(localStorage.getItem('user_id')),
                      isdeleted: 0, //static
                      company_id: JSON.parse(localStorage.getItem('company_id')),
                    };
                    this.authService.createWorkOrder(jobs_data)
                      .subscribe((data: any) => {
                        if (data.response == 'success') {
                          this.tickedId = data.last_insert_id;
                          localStorage.setItem('ticketId', String(this.tickedId));
                          localStorage.setItem('jobsData', JSON.stringify({
                            // selectedCustomer: this.selectednameid,
                            result: this.result,
                            service: this.service,
                            tmpClientService: this.tmpClientService,
                            selectednameid: this.selectednameid,
                            clientServiceData: this.clientServiceData,
                          }));

                          this.config.openSnackBar('Work Order Created');
                        }
                      });
                  }


                  // this.onChange(this.clientServiceData);



                });

              // if (localStorage.getItem('userData') != null) 

              // this.result = JSON.parse(localStorage.getItem('userData'));
              // if (this.result.address2 == null) {
              //   var address2 = '';
              // } else { address2 = this.result.address2; }
              // this.result.address = this.result.address1 + ' ' + address2;
              // this.selectednameid = this.result.id;
              // console.log("sdlefe", this.selectednameid);
              // this.onChange(this.selectednameid);
              // console.log("client filter", this.clientService);
              // let temp = this.clientService.filter((arr) => arr.customer_id == this.selectednameid);
              // this.tmpClientService = temp;
              // this.service=this.tmpClientService[0];
              // console.log("this.tmpClientService[0]", this.tmpClientService[0]);

              // this.onChange(this.tmpClientService[0].customer_id);
              // this.names = [JSON.parse(localStorage.getItem('userData'))];



            }

          },
          error => {
            console.log(error);
          }
        );
    }
    else {
      this.router.navigate(['/']);
    }
  }



  //here code to get onchange value
  getServiceDetailsFn(id) {
    var user = {
      service_id: id,
      company_id:JSON.parse(localStorage.getItem('company_id'))
    }

    this.authService.getClientServiceData(user)
      .subscribe(
        (data: any) => {
          this.service.id = data.response[0]['id'];
          this.service.company_id = data.response[0]['company_id'];
          this.service.customer_id = data.response[0]['customer_id'];
          this.service.first_name = data.response[0]['first_name'];
          this.service.address = data.response[0]['address1'] + " " + data.response[0]['address2'];
          this.service.last_name = data.response[0]['last_name'];
          this.service.email = data.response[0]['email'];
          this.service.phone = data.response[0]['phone'];
          this.service.altphone = data.response[0]['altphone'];
          this.service.address1 = data.response[0]['address1'];
          this.service.address2 = data.response[0]['address2'];
          this.service.city = data.response[0]['city'];
          this.service.state = data.response[0]['state'];
          this.service.zip = data.response[0]['postal_code'];
          this.service.saletax = data.response[0]['sales_tax'];
        },
        error => {
          console.log(error);
        }
      );
  }


  addDialog() {
    let dialogRef = this.matDialog.open(addCustomerComponent, {
      panelClass: 'adddialog',
      data: { showWorkOrder: false }

    });
    dialogRef.afterClosed().subscribe(result => {

      if (result) {

        this.result = result;
        if (result.address2 == null) {
          result.address2 = '';
        }
        this.result.address = result.address1 + " " + result.address2;
        this.selectednameid = this.result.customerId;

      }
      // this.initCustFunction();

      // this.initFunction(false);

      var user = {
        user_id: JSON.parse(localStorage.getItem('user_id')),
        company_id: JSON.parse(localStorage.getItem('company_id'))
      }
      this.authService.getClientService(user)
        .subscribe(
          (data: any) => {
            this.clientService = [];
            for (let i = 0; i < data.response.length; i++) {
              let name = data.response[i].first_name + ' ' + data.response[i].last_name;
              let arrayText = {
                id: data.response[i].id,
                customer_id: data.response[i].customer_id,
                name: name,
              }
              this.clientService.push(arrayText);
            }
            let temp = this.clientService.filter((arr) => arr.customer_id == this.selectednameid);
            this.tmpClientService = temp;
            this.clientServiceData = this.tmpClientService[0].id;
            this.getServiceDetailsFn(this.clientServiceData);
          });


      if (result == true) {
        return;
      } else {
        let user = {
          user_id: JSON.parse(localStorage.getItem('user_id')),
          company_id: JSON.parse(localStorage.getItem('company_id'))
        }
        this.authService.getAllCustomer(user)
          .subscribe(
            data => {
              this.names = data['response'];
            },
            error => {
              // console.log(error);
            }
          );
      }

    });



  }
  editDialog() {
    let dialogRef = this.matDialog.open(editCustomerComponent, {
      panelClass: 'editdialog',
      data: { id: this.selectednameid, showWorkOrder: false }
    });
    dialogRef.afterClosed().subscribe(resultdata => {
      //console.log(this.names);
      if (resultdata) {
        this.result = resultdata;
        this.result.address = resultdata.address1 + " " + resultdata.address2;
        for (let i = 0; i < this.names.length; i++) {
          if (this.names[i].id == resultdata.id) {
            this.names.splice(i, 1);
          }
        }
        this.names.push(resultdata);
        this.selectednameid = resultdata.id;
      }
    });
  }


  addServiceDialog() {

    if (this.selectednameid == '' || this.selectednameid == undefined) {
      // this.disableEditSrvc=true;
      this.config.openSnackBar('Please Select A Customer');
      return
    }

    let dialogRef = this.matDialog.open(addServiceComponent, {
      panelClass: 'editdialog',
      data: { customerID: this.selectednameid }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.service = result;

        if (result.address2 == null || result.address2 == 'null') {
          result.address2 = '';
        }

        this.service.address = result.address1 + " " + result.address2;

        this.clientServiceData = result.id;

        this.initFunction(false);
      }

    });

  }

  editServiceDialog() {
    if (this.service.id == '' || this.service.id == undefined) {
      // this.disableEditSrvc=true;
      this.config.openSnackBar('Please Select A Customer');
      return
    }
    let dialogRef = this.matDialog.open(editServiceComponent, {
      panelClass: 'editdialog',
      data: { response: this.service }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        for (let i = 0; i < this.tmpClientService.length; i++) {
          if (this.tmpClientService[i].id == result.id) {
            this.tmpClientService.splice(i, 1);
          }
        }
        this.service = result;
        this.service.address = result.address1 + " " + result.address2;
        let arrayText = {
          id: result.id,
          customer_id: result.customer_id,
          name: result.first_name + " " + result.last_name,
        }

        this.tmpClientService.push(arrayText);
        this.clientServiceData = result.id;
        this.initCustFunction();
      }
    });
  }

         editServiceDialog()
    {
        this.matDialog.open(editServiceComponent, {
            panelClass: 'editdialog',
        });
    }

       addScheduleDialog()
    {
        this.matDialog.open(addScheduleComponent, {
            panelClass: 'adddialog',
        });
    }

  updateWorkOrder() {
    // var today = new Date();
    // var dd = today.getDate();
    // var mm = today.getMonth() + 1; //January is 0!

    // var yyyy = today.getFullYear();
    // if (dd < 10) {
    //   let day = '0' + dd;
    // }
    // if (mm < 10) {
    //   let month = '0' + mm.toString;
    // }
    // let date = yyyy + '-' + mm + '-' + dd;

    let date = this.scheduledDateValue.substr(0, this.scheduledDateValue.indexOf('T'));

    let updateObj = {
      last_insert_id: this.tickedId,
      status: this.statusValue,
      category_id: this.categoryValue,
      short_description: this.shortDescription,
      description: this.description,
      date_scheduled: date,
      status_flg: 1, //static
      company_id: JSON.parse(localStorage.getItem('company_id'))
    }
    this.authService.updateWorkOrder(updateObj)
      .subscribe((data: any) => {
        if (data.response == 'success') {
          this.config.openSnackBar('Work order updated!');
          // this.router.navigate[''];
        }
        else {
          this.config.openSnackBar('Error! Work order not updated!');
        }
      });
  }

  public onAction(event: any) {
    console.log(event);
    this.actionLog += "\n currentFiles: " + this.getFileNames(event.currentFiles);
    console.log(this.actionLog);
  }
  public onAdded(event: any) {
    this.actionLog += "\n FileInput: " + event.id;
    this.actionLog += "\n Action: File added";
  }
  public onRemoved(event: any) {
    this.actionLog += "\n FileInput: " + event.id;
    this.actionLog += "\n Action: File removed";
  }
  public onCouldNotRemove(event: any) {
    this.actionLog += "\n FileInput: " + event.id;
    this.actionLog += "\n Action: Could not remove file";
  }

  private getFileNames(files: File[]): string {
    let names = files.map(file => file.name);
    return names ? names.join(", ") : "No files currently added.";
  }

  serviceCustomers = [
    {
      value: 'serv-1',
      viewValue: 'Bill Rydell'
    },
    {
      value: 'serv-2',
      viewValue: 'Mark Tycon'
    },
    {
      value: 'serv-3',
      viewValue: 'Rydell Mark'
    }
  ];
  customerCustomers = [
    {
      value: 'serv-1',
      viewValue: 'Jason Levy'
    },
    {
      value: 'serv-2',
      viewValue: 'Mark Tycon'
    },
    {
      value: 'serv-3',
      viewValue: 'Rydell Mark'
    }
  ];

  taxrates = [
    {
      value: '',
      viewValue: 'option1'
    },
    {
      value: '',
      viewValue: 'option1'
    },
    {
      value: '',
      viewValue: 'option1'
    }
  ];
  taxratedrafts = [
    {
      value: '',
      viewValue: 'option1'
    },
    {
      value: '',
      viewValue: 'option1'
    },
    {
      value: '',
      viewValue: 'option1'
    }
  ];

  status = [                  // added by r
    {
      value: 1,
      viewValue: 'New'
    },
    {
      value: 2,
      viewValue: 'Open'
    },
    {
      value: 3,
      viewValue: 'In-Progress'
    },
    {
      value: 4,
      viewValue: 'Closed'
    },
    {
      value: 5,
      viewValue: 'Withdrawn'
    }
  ];

  changeTab(data) {           // added by r
    this.tabObj = data;
  }

  changeRadio(data) {
    this.radioObj = data;
  }

  categorys = [
    {
      value: 1,
      viewValue: 'Category1'
    },
    {
      value: 2,
      viewValue: 'Category2'
    },
    {
      value: 3,
      viewValue: 'Category3'
    }
  ];



}




export interface Element {
  quantity: string;
  name: string;
  description: string;
  cost: string;
  rate: string;
  amount: number;
  // paymentreceived: string;
  // invoice:string;
  // paymentmethod:string;
  // ref:string;
  // memo:string;

}

export interface Elementp {
  paymentreceived: string;
  invoice: string;
  paymentmethod: string;
  ref: string;
  memo: string;
  amount: number;
}
export interface Elementpp {
  date: string;
  time: string;
  assignee: string;
  job: string;
}

export interface Elementppp {
  date: string;
  period: string;
  sentto: string;
  payment: string;
  invoice: string;
}


const ELEMENT_DATA: Element[] = [
  { quantity: '3', name: 'Serice', description: 're-plum all pipes', cost: '$10', rate: '$100', amount: 101.67, },
  { quantity: '1', name: 'Product', description: 'Pipe Fitting', cost: '$5', rate: '0', amount: 5.05 },
];


const PAYMENT_DATA: Elementp[] = [
  { paymentreceived: '2/29/2018', invoice: '28', paymentmethod: '2/29/2018', amount: 100.50, ref: '101', memo: '' },
  { paymentreceived: '2/29/2018', invoice: '28', paymentmethod: '2/29/2018', amount: 100.50, ref: '101', memo: '' },
  { paymentreceived: '2/29/2018', invoice: '28', paymentmethod: '2/29/2018', amount: 100.50, ref: '101', memo: '' },
  { paymentreceived: '2/29/2018', invoice: '28', paymentmethod: '2/29/2018', amount: 100.50, ref: '101', memo: '' },
  { paymentreceived: '2/29/2018', invoice: '28', paymentmethod: '2/29/2018', amount: 100.50, ref: '101', memo: '' },
  { paymentreceived: '2/29/2018', invoice: '28', paymentmethod: '2/29/2018', amount: 100.50, ref: '101', memo: '' },
  { paymentreceived: '2/29/2018', invoice: '28', paymentmethod: '2/29/2018', amount: 100.50, ref: '101', memo: '' },
  { paymentreceived: '2/29/2018', invoice: '28', paymentmethod: '2/29/2018', amount: 100.50, ref: '101', memo: '' },
  { paymentreceived: '2/29/2018', invoice: '28', paymentmethod: '2/29/2018', amount: 100.50, ref: '101', memo: '' },
  { paymentreceived: '2/29/2018', invoice: '28', paymentmethod: '2/29/2018', amount: 100.50, ref: '101', memo: '' },
  { paymentreceived: '2/29/2018', invoice: '28', paymentmethod: '2/29/2018', amount: 100.50, ref: '101', memo: '' },
];

const PENDINGAPP_DATA: Elementpp[] = [
  { date: '02/04/2018 ', time: '8:00am', assignee: 'Jason Levy', job: '2357' },
  { date: '02/04/2018 ', time: '8:00am', assignee: 'Jason Levy', job: '2357' },
  { date: '02/04/2018 ', time: '8:00am', assignee: 'Jason Levy', job: '2357' },
  { date: '02/04/2018 ', time: '8:00am', assignee: 'Jason Levy', job: '2357' },
  { date: '02/04/2018 ', time: '8:00am', assignee: 'Jason Levy', job: '2357' },
  { date: '02/04/2018 ', time: '8:00am', assignee: 'Jason Levy', job: '2357' },
  { date: '02/04/2018 ', time: '8:00am', assignee: 'Jason Levy', job: '2357' },
];

const INVOICESCH_DATA: Elementppp[] = [
  { date: '02/04/2018 ', period: 'Monthly', sentto: 'Sam Brown', payment: 'auto', invoice: '2357' },
  { date: '02/04/2018 ', period: 'Monthly', sentto: 'Sam Brown', payment: 'auto', invoice: '' },
  { date: '02/04/2018 ', period: 'Monthly', sentto: 'Sam Brown', payment: 'auto', invoice: '' },
  { date: '02/04/2018 ', period: 'Monthly', sentto: 'Sam Brown', payment: 'auto', invoice: '' },
  { date: '02/04/2018 ', period: 'Monthly', sentto: 'Sam Brown', payment: 'auto', invoice: '' },
  { date: '02/04/2018 ', period: 'Monthly', sentto: 'Sam Brown', payment: 'auto', invoice: '' },
  { date: '02/04/2018 ', period: 'Monthly', sentto: 'Sam Brown', payment: 'auto', invoice: '' },


];
