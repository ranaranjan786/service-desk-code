import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { fuseAnimations } from '../../../core/animations';

@Component({
    templateUrl: './estimates.component.html',
    styleUrls  : ['./estimates.component.scss'],
    animations : fuseAnimations
})
export class EstimatesComponent
{

}
