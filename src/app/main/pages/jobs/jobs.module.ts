import { NgModule } from '@angular/core';
import { Component } from '@angular/core';
import { SharedModule } from '../../../core/modules/shared.module';
import { RouterModule } from '@angular/router';

import { JobsComponent } from './jobs.component';
import { FuseWidgetModule } from '../../../core/components/widget/widget.module';
import { Ng2FileInputModule } from 'ng2-file-input';
const routes = [
  {
    path: 'jobs',
    component: JobsComponent
  }
];

@NgModule({
  declarations: [
    JobsComponent
  ],
  imports: [
    SharedModule,
    RouterModule.forChild(routes),
    FuseWidgetModule,
    Ng2FileInputModule.forRoot()
  ]
})

export class JobsModule { }
