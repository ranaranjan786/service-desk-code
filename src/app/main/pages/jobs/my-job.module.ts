import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MyJobComponent } from  './my-job.component';

const routes = [
    {
        path     : 'myjob',
        component: MyJobComponent
    }
];

@NgModule({
    declarations: [
        MyJobComponent
    ],
    imports     : [
        RouterModule.forChild(routes)
    ]
})

export class MyJobModule
{

}
