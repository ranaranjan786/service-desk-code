import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { EstimatesComponent } from './estimates.component';

const routes = [
    {
        path     : 'estimates',
        component: EstimatesComponent
    }
];

@NgModule({
    declarations: [
        EstimatesComponent
    ],
    imports     : [
        RouterModule.forChild(routes)
    ]
})

export class EstimatesModule
{

}
