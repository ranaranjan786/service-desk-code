import { Component, OnInit } from '@angular/core';
import { fuseAnimations } from '../../../core/animations';
import { AuthServiceService } from '../../../../services/auth-service.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Config } from '../../../../config';

@Component({
  templateUrl: './myProfile.component.html',
  styleUrls: ['./myProfile.component.scss'],
  animations: fuseAnimations
})
export class MyProfileComponent {

  user = {

    email: "",
    first_name: "",
    last_name: '',
    passwordVal: undefined,
    confirm_password: undefined,
    current_password: undefined,
    phone: undefined,
    role: "Administrator",
    user_id: undefined,
    company_id: undefined

  };

  myForm: FormGroup;

  constructor(private authService: AuthServiceService, private config:Config) {

  }

  ngOnInit() {
    let obj = {
      company_id: JSON.parse(localStorage.getItem('company_id')),
      user_id: JSON.parse(localStorage.getItem('user_id'))
    }

    this.authService.userDetails(obj)
      .subscribe((data: any) => {
        this.user = data.response[0];
        this.user.role = "1";
        this.user.user_id = JSON.parse(localStorage.getItem('user_id'));
        // this.user.role="Administrator"
      });
  }

  update(user) {
    console.log("user", user);
    this.authService.updateUserDetails(user)
      .subscribe((data: any) => {
         if (data.response == "Updated Successfully") {
          this.config.openSnackBar('Updated Successfully');
        }
        else if(data.response =="Password did not match"){
          this.config.openSnackBar("Password did not match");
        }
        else if(data.response =="Current Password did not match"){
          this.config.openSnackBar("Current Password did not match");
        }

      });
  }

  checkUpdateValues(){
    return
  }

}



