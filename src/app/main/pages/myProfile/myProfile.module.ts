import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../../../core/modules/shared.module';
import { MyProfileComponent } from './myProfile.component';

const routes = [
    {
        path     : 'my-profile',
        component: MyProfileComponent
    }
];

@NgModule({
    declarations: [
        MyProfileComponent
    ],
    imports     : [
        RouterModule.forChild(routes),
        SharedModule
    ]
})

export class MyProfileModule
{

}
