import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { fuseAnimations } from '../../../core/animations';
import { setConfigFuseConfigService } from '../../../core/services/config.service';
import { AuthServiceService } from '../../../../services/auth-service.service';
import { Router } from '@angular/router';

@Component({
     templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  animations : fuseAnimations
})
export class HomeComponent
{
  constructor(
    private fuseConfig: setConfigFuseConfigService,
    private formBuilder: FormBuilder,
    private authService: AuthServiceService,
    private router: Router
){

}
  ngOnInit()
    {
        var token = localStorage.getItem('token');
        if(token!= null){
            var tokenVal= {
              tokenValue : token
            }
            this.authService.loginCheck(tokenVal)
            .subscribe(
                data => {
                  
                  //console.log("login response",data);
                  if(data['result'] == 'error'){
                    localStorage.setItem('token', '');
                    this.router.navigate(['/']);
                  }else{
                    this.router.navigate(['dashboard']);
                  }
                // console.log('token', data['response']);
                },
                error => {
                  console.log(error);
                }
            );
        }else{
          this.router.navigate(['/']);
        }
       
    }
}
