import { NgModule } from '@angular/core';
import { SharedModule } from '../../../core/modules/shared.module';
import { RouterModule, Routes } from '@angular/router';
import { FuseTodoComponent } from './myJobs.component';
import { TodoService } from './myJobs.service';
import { FuseTodoMainSidenavComponent } from './sidenavs/main/main-sidenav.component';
import { FuseTodoListItemComponent } from './myJobs-list/myJobs-list-item/myJobs-list-item.component';
import { FuseTodoListComponent } from './myJobs-list/myJobs-list.component';
import { FuseTodoDetailsComponent } from './myJobs-details/myJobs-details.component';

const routes: Routes = [
    {
        path     : 'my-job',
        component: FuseTodoComponent,
        resolve  : {
            todo: TodoService
        }
    },
    {
        path     : 'my-job/:todoId',
        component: FuseTodoComponent,
        resolve  : {
            todo: TodoService
        }
    },
    {
        path     : 'my-job/:tagHandle',
        component: FuseTodoComponent,
        resolve  : {
            todo: TodoService
        }
    },
    {
        path     : 'my-job/:tagHandle/:todoId',
        component: FuseTodoComponent,
        resolve  : {
            todo: TodoService 
        }
    },
    {
        path     : 'filter/:filterHandle',
        component: FuseTodoComponent,
        resolve  : {
            todo: TodoService
        }
    },
    {
        path     : 'filter/:filterHandle/:todoId',
        component: FuseTodoComponent,
        resolve  : {
            todo: TodoService
        }
    },
    // {
    //     path      : 'my-jobs',
    //     redirectTo: 'my-jobs'
    // }
];

@NgModule({
    declarations: [
        FuseTodoComponent,
        FuseTodoMainSidenavComponent,
        FuseTodoListItemComponent,
        FuseTodoListComponent,
        FuseTodoDetailsComponent
    ],
    imports     : [
        SharedModule,
        RouterModule.forChild(routes)
    ],
    providers   : [
        TodoService
    ]
})
export class FuseTodoModule
{
}
