import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { TodoService } from '../myJobs.service';
import { Todo } from '../myJobs.model';
import { Subscription } from 'rxjs/Subscription';
import { FormBuilder, FormGroup } from '@angular/forms';
import { FuseUtils } from '../../../../core/fuseUtils';
import { fuseAnimations } from '../../../../core/animations';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';

@Component({
    selector   : 'fuse-todo-details',
    templateUrl: './myJobs-details.component.html',
    styleUrls  : ['./myJobs-details.component.scss'],
    animations : fuseAnimations
})
export class FuseTodoDetailsComponent implements OnInit, OnDestroy
{
    todo: Todo;
    tags: any[];
    formType: string;
    todoForm: FormGroup;
    @ViewChild('titleInput') titleInputField;

    onFormChange: any;
    onCurrentTodoChanged: Subscription;
    onTagsChanged: Subscription;
    onNewTodoClicked: Subscription;

    constructor(
        private todoService: TodoService,
        private formBuilder: FormBuilder
    )
    {

    }

    ngOnInit()
    {
        // Subscribe to update the current todo
        this.onCurrentTodoChanged =
            this.todoService.onCurrentTodoChanged
                .subscribe(([todo, formType]) => {

                    if ( todo && formType === 'edit' )
                    {
                        this.formType = 'edit';

                        this.todo = todo;

                        this.todoForm = this.createTodoForm();

                        this.onFormChange =
                            this.todoForm.valueChanges
                                .debounceTime(500)
                                .distinctUntilChanged()
                                .subscribe(data => {
                                    this.todoService.updateTodo(data);
                                });
                    }
                });

        // Subscribe to update on tag change
        this.onTagsChanged =
            this.todoService.onTagsChanged
                .subscribe(labels => {
                    this.tags = labels;
                });

        // Subscribe to update on tag change
        this.onNewTodoClicked =
            this.todoService.onNewTodoClicked
                .subscribe(() => {
                    this.todo = new Todo({});
                    this.todo.id = FuseUtils.generateGUID();
                    this.formType = 'new';
                    this.todoForm = this.createTodoForm();
                    this.focusTitleField();
                    this.todoService.onCurrentTodoChanged.next([this.todo, 'new']);
                });
    }

    focusTitleField()
    {
        setTimeout(() => {
            this.titleInputField.nativeElement.focus();
        });
    }

    createTodoForm()
    {
        return this.formBuilder.group({
            'id'       : [this.todo.id],
            'first_name'    : [this.todo.first_name],
            'last_name'    : [this.todo.last_name],
            'date_scheduled': [this.todo.date_scheduled],
            'short_description'  : [this.todo.short_description],
            'description': [this.todo.description],
            'starred'  : [this.todo.starred],
            'important': [this.todo.important],
            'category_id'  : [this.todo.category_id],
            // 'tags'     : [this.todo.tags]
        });
    }

   







    toggleStar(event)
    {
        event.stopPropagation();

        this.todo.toggleStar();

        this.todoService.updateTodo(this.todo);
    }

    toggleImportant(event)
    {
        event.stopPropagation();

        this.todo.toggleImportant();

        this.todoService.updateTodo(this.todo);
    }

    /**
     * Toggle Completed
     * @param event
     */
    toggleCompleted(event)
    {
        event.stopPropagation();

        // this.todo.toggleCompleted();

        this.todoService.updateTodo(this.todo);
    }

    /**
     * Toggle Deleted
     * @param event
     */
    toggleDeleted(event)
    {
        event.stopPropagation();

        // this.todo.toggleDeleted();

        this.todoService.updateTodo(this.todo);
    }

    toggleTagOnTodo(tagId)
    {
        this.todoService.toggleTagOnTodo(tagId, this.todo);
    }

    addTodo()
    {
        this.todoService.updateTodo(this.todoForm.getRawValue());
    }

    ngOnDestroy()
    {
        if ( this.onFormChange )
        {
            this.onFormChange.unsubscribe();
        }

        this.onCurrentTodoChanged.unsubscribe();

        this.onNewTodoClicked.unsubscribe();
    }
}
