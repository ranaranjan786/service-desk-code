export class Todo {
    id: string;
    first_name: string;
    last_name: string;
    short_description: string;
    description: string;
    date_scheduled: string;
    // dueDate: boolean;
    category_id: boolean;
    starred: boolean;
    important: boolean;
    client_id: number;
    company_id: number;
    customer_id: number;
    date_created: string;
    date_updated: string;
    isdeleted: number;
    status: string;
    status_flg: number;
    user_id: number;
   





    // deleted: boolean;
    tags: [
        {
            'id': number,
            'name': string,
            'label': string,
            'color': string
        }
        ];

    constructor(todo) {
        {
            this.id = todo.id;
            this.first_name = todo.first_name;
            this.last_name = todo.last_name;
            this.date_scheduled = todo.date_scheduled;
            this.category_id = todo.category_id;
            // this.completed = todo.completed;
            this.starred = true;
            this.important = todo.important;
            this.client_id=todo.client_id;
            this.company_id=todo.company_id;
            this.customer_id=todo.customer_id;
            this.date_created=todo.date_created;
            this.date_updated=todo.date_updated;
            this.isdeleted=todo.isdeleted;
            this.status=todo.status;
            this.status_flg=todo.status_flg;
            this.user_id=todo.user_id;
            this.short_description= todo.short_description;
            this.description= todo.description;
            this.tags=  [
                {
                    'id': 1,
                    'name': 'Job',
                    'label': 'Job lable',
                    'color': 'Orange'
                }
                ]
      
        }
    }

    toggleStar() {
        this.starred = !this.starred;
    }

    toggleImportant() {
        this.important = !this.important;
    }

    // toggleCompleted()
    // {
    //     this.completed = !this.completed;
    // }

    // toggleDeleted()
    // {
    //     this.deleted = !this.deleted;
    // }
}
