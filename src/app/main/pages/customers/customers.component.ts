import { Component, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material';
import { MatPaginatorModule } from '@angular/material/paginator';
import { DataSource } from '@angular/cdk/collections';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { Router } from '@angular/router';
import { GlobalService } from '../../../../services/global-service';
import { AuthServiceService } from '../../../../services/auth-service.service';
import { Config } from '../../../../config';

@Component({
  selector: 'app-table',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0', visibility: 'hidden' })),
      state('expanded', style({ height: '*', visibility: 'visible' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class CustomersComponent {
  displayedColumns = ['add', 'name', 'email', 'phone', 'address', 'delete'];
  // dataSource = new ExpendDataSource();
  dataSource = new MatTableDataSource([]);
  isExpansionDetailRow = (i: number, row: Object) => row.hasOwnProperty('detailRow');
  expandedElement: any;
  customerData = [];
  customerApiData = [];

  constructor(private router: Router,
    private globalService: GlobalService,
    private authService: AuthServiceService,
    private config: Config) {

  }

  ngOnInit() {
    let obj = {
      user_id: JSON.parse(localStorage.getItem('user_id')),
      company_id: JSON.parse(localStorage.getItem('company_id')),
    }
    this.authService.customerServiceDetails(obj)
      .subscribe((data: any) => {
        this.customerApiData = data.response;
        data.response.map(data => {
          this.customerData.push(data.customer);
        });
        this.dataSource = new MatTableDataSource(this.customerData);
        // console.log(" this.this.dataSource", this.dataSource);

        // console.log("this.customerData", this.customerData);


      });
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
    // this.dataSource.data = this.customerData.data;
  }


  edit(element) {
    // console.log("element", element);
    var tempData = this.customerApiData.filter((arr) => {
      return arr.customer.id == element.id;
    });
    // console.log('temp', tempData);


    this.globalService.getCurrentCustomerDetailedData(tempData);
    this.router.navigate(['customer-detail']);
  }

  delete(element) {
    // console.log("element", element);

    let obj = {
      customer_id: element.id,
      company_id: JSON.parse(localStorage.getItem('company_id'))
    };
    this.authService.deleteCustomer(obj)
      .subscribe((data: any) => {

        if (data.response == "success") {
          this.customerData.forEach((arr, index) => {
            if (arr.id == element.id) {
              this.customerData.splice(index, 1);
            }
          }
          );
          this.config.openSnackBar('Deleted Successfully!');
        }
        else {
          this.config.openSnackBar('Error!');
        }
      })
    this.dataSource = new MatTableDataSource(this.customerData);
    return;
  }



  selectedRow(element) {
    // console.log("element", element);
  }



}

// export interface Element {

//   name: string;
//   email: string;
//   phone: string;
//   address: string;

// }

const data = [
  { name: 'Giacomo Guilizzoni', email: 'Giacoma@gmail.com', phone: '703-555-5555', address: '2014 Dun St yuma AZ 33392' },
  { name: 'Marco Botton', email: 'Marco@gmail.com', phone: '703-555-5555', address: '2014 Dun St yuma AZ 33392' },
  { name: 'Valerie Liberty', email: 'Valerie@gmail.com', phone: '703-555-5555', address: '2014 Dun St yuma AZ 33392' },
  { name: 'Ken Rogers', email: 'Ken@gmail.com', phone: '703-555-5555', address: '2014 Dun St yuma AZ 33392' },
  { name: 'Giacomo Guilizzoni', email: 'Giacoma@gmail.com', phone: '703-555-5555', address: '2014 Dun St yuma AZ 33392' },
  { name: 'Marco Botton', email: 'Marco@gmail.com', phone: '703-555-5555', address: '2014 Dun St yuma AZ 33392' },
  { name: 'Valerie Liberty', email: 'Valerie@gmail.com', phone: '703-555-5555', address: '2014 Dun St yuma AZ 33392' },
  { name: 'Ken Rogers', email: 'Ken@gmail.com', phone: '703-555-5555', address: '2014 Dun St yuma AZ 33392' },
  { name: 'Giacomo Guilizzoni', email: 'Giacoma@gmail.com', phone: '703-555-5555', address: '2014 Dun St yuma AZ 33392' },
  { name: 'Marco Botton', email: 'Marco@gmail.com', phone: '703-555-5555', address: '2014 Dun St yuma AZ 33392' },
  { name: 'Valerie Liberty', email: 'Valerie@gmail.com', phone: '703-555-5555', address: '2014 Dun St yuma AZ 33392' },
  { name: 'Ken Rogers', email: 'Ken@gmail.com', phone: '703-555-5555', address: '2014 Dun St yuma AZ 33392' },
  { name: 'Giacomo Guilizzoni', email: 'Giacoma@gmail.com', phone: '703-555-5555', address: '2014 Dun St yuma AZ 33392' },
  { name: 'Marco Botton', email: 'Marco@gmail.com', phone: '703-555-5555', address: '2014 Dun St yuma AZ 33392' },
  { name: 'Valerie Liberty', email: 'Valerie@gmail.com', phone: '703-555-5555', address: '2014 Dun St yuma AZ 33392' }
]

export class ExpendDataSource extends DataSource<any> {
  /** Connect function called by the table to retrieve one stream containing the data to render. */
  connect(): Observable<Element[]> {
    const rows = [];
    data.forEach(element => rows.push(element, { detailRow: true, element }));
    return Observable.of(rows);
  }

  disconnect() { }
}
