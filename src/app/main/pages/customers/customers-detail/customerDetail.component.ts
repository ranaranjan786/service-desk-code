import { Component, OnInit, NgZone } from '@angular/core';
import { FormControl } from '@angular/forms';
import { fuseAnimations } from '../../../../core/animations';
import { MatTableDataSource } from '@angular/material';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';


import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { GlobalService } from '../../../../../services/global-service';
import { Router } from '@angular/router';
import { AuthServiceService } from '../../../../../services/auth-service.service';
import { Config } from '../../../../../config';
import { Validation } from '../../../../../services/validation';

@Component({
  templateUrl: './customerDetail.component.html',
  styleUrls: ['./customerDetail.component.scss'],
  animations: fuseAnimations
})

export class CustomerDetailComponent {
  tabObj: string;
  radioObj: string;
  public actionLog: string = "";
  serviceData = [];
  customer;
  service;
  serviceId=0;
  showErrorMessage:boolean=false;
  errorMessage='';
  // displayedColumns = ['approved', 'quantity', 'name', 'description', 'cost', 'rate', 'amount', 'tax', 'delete'];
  // dataSource = new MatTableDataSource(ELEMENT_DATA);

  // paymentColumns = ['paymentreceived', 'invoice', 'paymentmethod', 'amount', 'ref', 'memo'];
  // paymentdetails = new MatTableDataSource(PAYMENT_DATA);

  // pendingschColumns = ['date', 'time', 'assignee', 'job'];
  // pendingappschdule = new MatTableDataSource(PENDINGAPP_DATA);

  // invoicedataColumns = ['date', 'period', 'sentto', 'payment', 'invoice'];
  // invoicedata = new MatTableDataSource(INVOICESCH_DATA);

  // date = new FormControl(new Date());
  // serializedDate = new FormControl((new Date()).toISOString());

  // selectedValue: string;

  constructor(private globalService: GlobalService,
     private router: Router,
    private authService:AuthServiceService,
  private config:Config,
  private validate:Validation) {


  }

  ngOnInit() {
    // if( ){

    // }

    this.globalService.currentCustomerDetailedData
      .subscribe((data: any) => {
        this.serviceData = data[0].service;
        this.service = this.serviceData[0];
        if (this.service.address2 == "null" || this.service.address2 == null) {
          this.service.address2 = '';
        }
        this.service.address = this.service.address1 + ' ' + this.service.address2;
        
        this.serviceId=this.service.id;
        // console.log("serviceCustomers", this.serviceCustomers);
        this.customer = data[0].customer;
        this.customer.name = this.customer.first_name + ' ' + this.customer.last_name;
        if (this.customer.address2 == "null") {
          this.customer.address2 = '';
        }
        this.customer.address = this.customer.address1 + ' ' + this.customer.address2;
        this.service.zip=this.service.postal_code;
        this.customer.zip=this.customer.postal_code;



        // this.globalService.getCurrentCustomerDetailedData(null);
      });

  }

  getServiceData(serviceId) {
    let temp = this.serviceData.filter(arr => {
      return arr.id == serviceId;

    });
    this.service = temp[0];
    if (this.service.address2 == "null" || this.service.address2 == null) {
      this.service.address2 = '';
    }
    this.service.address = this.service.address1 + ' ' + this.service.address2;
    this.service.zip=this.service.postal_code;
    
  }




  getCustomerServiceUpdate(customer,service){
    let obj={
      customer:customer,
      service:service
    }

    this.authService.getCustomerServiceUpdate(obj)
    .subscribe((data:any)=>{
      if(data.response=="success"){
        this.config.openSnackBar('Details Updated!');
      }
      else{
        this.config.openSnackBar('Error!');
      }
    });

  }

  createWorkOrder(customer){
    // if (!this.validate.checkValidations(customer)) {
    //   return;
    // }
    this.globalService.getCustomerData(customer);
    // this.authService.createWorkOrder();
    this.router.navigate(['jobs']);
  }


  public onAction(event: any) {
    console.log(event);
    this.actionLog += "\n currentFiles: " + this.getFileNames(event.currentFiles);
    console.log(this.actionLog);
  }
  public onAdded(event: any) {
    this.actionLog += "\n FileInput: " + event.id;
    this.actionLog += "\n Action: File added";
  }
  public onRemoved(event: any) {
    this.actionLog += "\n FileInput: " + event.id;
    this.actionLog += "\n Action: File removed";
  }
  public onCouldNotRemove(event: any) {
    this.actionLog += "\n FileInput: " + event.id;
    this.actionLog += "\n Action: Could not remove file";
  }

  private getFileNames(files: File[]): string {
    let names = files.map(file => file.name);
    return names ? names.join(", ") : "No files currently added.";
  }
  // states = [
  //   'Alabama', 'Alaska', 'Arizona', 'Arkansas', 'California', 'Colorado', 'Connecticut', 'Delaware',
  //   'Florida', 'Georgia', 'Hawaii', 'Idaho', 'Illinois', 'Indiana', 'Iowa', 'Kansas', 'Kentucky',
  //   'Louisiana', 'Maine', 'Maryland', 'Massachusetts', 'Michigan', 'Minnesota', 'Mississippi',
  //   'Missouri', 'Montana', 'Nebraska', 'Nevada', 'New Hampshire', 'New Jersey', 'New Mexico',
  //   'New York', 'North Carolina', 'North Dakota', 'Ohio', 'Oklahoma', 'Oregon', 'Pennsylvania',
  //   'Rhode Island', 'South Carolina', 'South Dakota', 'Tennessee', 'Texas', 'Utah', 'Vermont',
  //   'Virginia', 'Washington', 'West Virginia', 'Wisconsin', 'Wyoming'
  // ];
  // serviceCustomers = [
  //   {
  //     value: 'serv-1',
  //     viewValue: 'Bill Rydell'
  //   },
  //   {
  //     value: 'serv-2',
  //     viewValue: 'Mark Tycon'
  //   },
  //   {
  //     value: 'serv-3',
  //     viewValue: 'Rydell Mark'
  //   }
  // ];
  // customerCustomers = [
  //   {
  //     value: 'serv-1',
  //     viewValue: 'Jason Levy'
  //   },
  //   {
  //     value: 'serv-2',
  //     viewValue: 'Mark Tycon'
  //   },
  //   {
  //     value: 'serv-3',
  //     viewValue: 'Rydell Mark'
  //   }
  // ];

  // taxrates = [
  //   {
  //     value: '',
  //     viewValue: 'option1'
  //   },
  //   {
  //     value: '',
  //     viewValue: 'option1'
  //   },
  //   {
  //     value: '',
  //     viewValue: 'option1'
  //   }
  // ];
  // taxratedrafts = [
  //   {
  //     value: '',
  //     viewValue: 'option1'
  //   },
  //   {
  //     value: '',
  //     viewValue: 'option1'
  //   },
  //   {
  //     value: '',
  //     viewValue: 'option1'
  //   }
  // ];


  // status = [                  // added by r
  //   {
  //     value: 'openD',
  //     viewValue: 'Open'
  //   },
  //   {
  //     value: 'approvD',
  //     viewValue: 'Approved'
  //   },
  //   {
  //     value: 'draftD',
  //     viewValue: 'Draft'
  //   }
  // ];

  // changeTab(data) {           // added by r
  //   this.tabObj = data;
  // }

  // changeRadio(data) {
  //   this.radioObj = data;
  // }

  // catagorys = [
  //   {
  //     value: '',
  //     viewValue: 'Catagory1'
  //   },
  //   {
  //     value: '',
  //     viewValue: 'Catagory2'
  //   },
  //   {
  //     value: '',
  //     viewValue: 'Catagory3'
  //   }
  // ];



}




export interface Element {
  quantity: string;
  name: string;
  description: string;
  cost: string;
  rate: string;
  amount: number;
  // paymentreceived: string;
  // invoice:string;
  // paymentmethod:string;
  // ref:string;
  // memo:string;

}

export interface Elementp {
  paymentreceived: string;
  invoice: string;
  paymentmethod: string;
  ref: string;
  memo: string;
  amount: number;
}
export interface Elementpp {
  date: string;
  time: string;
  assignee: string;
  job: string;
}

export interface Elementppp {
  date: string;
  period: string;
  sentto: string;
  payment: string;
  invoice: string;
}


const ELEMENT_DATA: Element[] = [
  { quantity: '3', name: 'Serice', description: 're-plum all pipes', cost: '$10', rate: '$100', amount: 101.67, },
  { quantity: '1', name: 'Product', description: 'Pipe Fitting', cost: '$5', rate: '0', amount: 5.05 },
];


const PAYMENT_DATA: Elementp[] = [
  { paymentreceived: '2/29/2018', invoice: '28', paymentmethod: '2/29/2018', amount: 100.50, ref: '101', memo: '' },
  { paymentreceived: '2/29/2018', invoice: '28', paymentmethod: '2/29/2018', amount: 100.50, ref: '101', memo: '' },
  { paymentreceived: '2/29/2018', invoice: '28', paymentmethod: '2/29/2018', amount: 100.50, ref: '101', memo: '' },
  { paymentreceived: '2/29/2018', invoice: '28', paymentmethod: '2/29/2018', amount: 100.50, ref: '101', memo: '' },
  { paymentreceived: '2/29/2018', invoice: '28', paymentmethod: '2/29/2018', amount: 100.50, ref: '101', memo: '' },
  { paymentreceived: '2/29/2018', invoice: '28', paymentmethod: '2/29/2018', amount: 100.50, ref: '101', memo: '' },
  { paymentreceived: '2/29/2018', invoice: '28', paymentmethod: '2/29/2018', amount: 100.50, ref: '101', memo: '' },
  { paymentreceived: '2/29/2018', invoice: '28', paymentmethod: '2/29/2018', amount: 100.50, ref: '101', memo: '' },
  { paymentreceived: '2/29/2018', invoice: '28', paymentmethod: '2/29/2018', amount: 100.50, ref: '101', memo: '' },
  { paymentreceived: '2/29/2018', invoice: '28', paymentmethod: '2/29/2018', amount: 100.50, ref: '101', memo: '' },
  { paymentreceived: '2/29/2018', invoice: '28', paymentmethod: '2/29/2018', amount: 100.50, ref: '101', memo: '' },
];

const PENDINGAPP_DATA: Elementpp[] = [
  { date: '02/04/2018 ', time: '8:00am', assignee: 'Jason Levy', job: '2357' },
  { date: '02/04/2018 ', time: '8:00am', assignee: 'Jason Levy', job: '2357' },
  { date: '02/04/2018 ', time: '8:00am', assignee: 'Jason Levy', job: '2357' },
  { date: '02/04/2018 ', time: '8:00am', assignee: 'Jason Levy', job: '2357' },
  { date: '02/04/2018 ', time: '8:00am', assignee: 'Jason Levy', job: '2357' },
  { date: '02/04/2018 ', time: '8:00am', assignee: 'Jason Levy', job: '2357' },
  { date: '02/04/2018 ', time: '8:00am', assignee: 'Jason Levy', job: '2357' },
];

const INVOICESCH_DATA: Elementppp[] = [
  { date: '02/04/2018 ', period: 'Monthly', sentto: 'Sam Brown', payment: 'auto', invoice: '2357' },
  { date: '02/04/2018 ', period: 'Monthly', sentto: 'Sam Brown', payment: 'auto', invoice: '' },
  { date: '02/04/2018 ', period: 'Monthly', sentto: 'Sam Brown', payment: 'auto', invoice: '' },
  { date: '02/04/2018 ', period: 'Monthly', sentto: 'Sam Brown', payment: 'auto', invoice: '' },
  { date: '02/04/2018 ', period: 'Monthly', sentto: 'Sam Brown', payment: 'auto', invoice: '' },
  { date: '02/04/2018 ', period: 'Monthly', sentto: 'Sam Brown', payment: 'auto', invoice: '' },
  { date: '02/04/2018 ', period: 'Monthly', sentto: 'Sam Brown', payment: 'auto', invoice: '' },


];
