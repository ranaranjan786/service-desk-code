import { NgModule } from '@angular/core';
import { Component } from '@angular/core';
import { SharedModule } from '../../../../core/modules/shared.module';
import { RouterModule } from '@angular/router';

import { CustomerDetailComponent } from './customerDetail.component';
import { FuseWidgetModule } from '../../../../core/components/widget/widget.module';
const routes = [
  {
    path: 'customer-detail',
    component: CustomerDetailComponent
  }
];

@NgModule({
  declarations: [
    CustomerDetailComponent
  ],
  imports: [
    SharedModule,
    RouterModule.forChild(routes),
    FuseWidgetModule,
  ]
})

export class CustomerDetailModule { }
