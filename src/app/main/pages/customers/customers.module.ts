import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CustomersComponent } from './customers.component';
import { SharedModule } from '../../../core/modules/shared.module';
import {MatPaginatorModule } from '@angular/material';
const routes = [
    {
        path     : 'customers',
        component: CustomersComponent
    }
];

@NgModule({
    declarations: [
        CustomersComponent
    ],
    imports     : [
        SharedModule,
        MatPaginatorModule,
        RouterModule.forChild(routes)
    ]
})

export class CustomersModule
{

}
