import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { fuseAnimations } from '../../../core/animations';

@Component({
    templateUrl: './calendar.component.html',
    styleUrls  : ['./calendar.component.scss'],
    animations : fuseAnimations
})
export class CalendarComponent
{

}
