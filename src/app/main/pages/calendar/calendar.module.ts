import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { CalendarComponent } from './calendar.component';

const routes = [
    {
        path     : 'calendar',
        component: CalendarComponent
    }
];

@NgModule({
    declarations: [
        CalendarComponent
    ],
    imports     : [
        RouterModule.forChild(routes)
    ]
})

export class CalendarPageModule
{

}
