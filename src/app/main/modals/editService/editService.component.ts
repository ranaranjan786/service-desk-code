import { Component, OnInit, Inject } from '@angular/core';
import { FormControl } from '@angular/forms';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { fuseAnimations } from '../../../core/animations';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { setConfigFuseConfigService } from '../../../core/services/config.service';
import { AuthServiceService } from '../../../../services/auth-service.service';
import { Router } from '@angular/router';
import { Config } from '../../../../config';
import { Validation } from '../../../../services/validation';

@Component({
    selector: 'editdialog',
    templateUrl: './editService.component.html',
    styleUrls: ['./editService.component.scss'],
    animations: fuseAnimations
})
export class editServiceComponent implements OnInit {
    service: any = {
        id: '',
        customer_id: '',
        company_id: '',
        address: '',
        phone: '',
        email: '',
        state: '',
        city: '',
        address1: '',
        altphone: '',
        first_name: '',
        last_name: '',
        address2: '',
        zip: '',
        saletax: undefined
    };
    errorMessage = '';
    showErrorMessage: boolean = false;
    constructor(
        @Inject(MAT_DIALOG_DATA)
        public data: any,
        private authService: AuthServiceService,
        private router: Router,
        private mat: MatDialogRef<editServiceComponent>,
        private config: Config,
        private validate: Validation


    ) {

    }

    ngOnInit() {
        this.service = this.data.response;
        console.log("response", this.service);
    }



    submitfn(service) {

        if (!this.validate.checkValidations(service)) {
            return;
        }

        if (service.altphone == undefined) {
            service.altphone = '';
        }
        if (service.address2 == undefined) {
            service.address2 = '';
        }

        service.status_id = 1; //static
        service.enteredbyuser_ID = JSON.parse(localStorage.getItem('user_id'));
        service.company_id=JSON.parse(localStorage.getItem('company_id'));

        //    this.service.push(this.data.response.id);
        this.authService.editClientService(this.service)
            .subscribe(
                (data) => {
                    console.log('editdata', data);
                    if (data['response'] == 'Email already exists!!') {
                        this.errorMessage = 'Email already exists!!';
                        this.showErrorMessage = true;

                    } else {
                        this.mat.close(service);
                        this.config.openSnackBar('Service Updated Successfully');
                        this.showErrorMessage = false;
                    }
                    // console.log("login response",data);
                    // localStorage.setItem("addservice",data.response);

                },
                error => {
                    console.log(error);
                }
            );

    }

}

