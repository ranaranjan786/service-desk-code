import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { fuseAnimations } from '../../../core/animations';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Inject } from '@angular/core';
import { AuthServiceService } from '../../../../services/auth-service.service';
import { Router } from '@angular/router';
import { Config } from '../../../../config';
import { Validation } from '../../../../services/validation';
import { GlobalService } from '../../../../services/global-service';

@Component({
  selector: 'editdialog',
  templateUrl: './editCustomer.component.html',
  styleUrls: ['./editCustomer.component.scss'],
  animations: fuseAnimations
})
export class editCustomerComponent implements OnInit {

  showWorkOrder: boolean;

  constructor(private mat: MatDialogRef<editCustomerComponent>,
    private authService: AuthServiceService,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private router: Router,
    private config: Config,
    private validate: Validation,
    private globalService: GlobalService


  ) { }

  result: any = [];
  errorMessage = '';
  showErrorMessage: boolean = false;
  ngOnInit() {

    let custId = this.data['id'];
    this.showWorkOrder = this.data['showWorkOrder'];
    let idParam = {
      id: custId,
      company_id: JSON.parse(localStorage.getItem('company_id'))
    }
    this.authService.editCustomerdata(idParam)
      .subscribe(
        data => {
          this.result = data[0];
          this.result.zip = this.result.postal_code;
          this.result.customerId = custId;
        },
        error => {
          console.log(error);
        }
      );
  }

  updateCustomer(result) {

    if (!this.validate.checkValidations(result)) {
      return;
    }

    result.status_id = 1; //static
    result.enteredbyuser_ID = JSON.parse(localStorage.getItem('user_id'));
    result.company_id= JSON.parse(localStorage.getItem('company_id'));

    this.authService.updateCustomerdata(result)
      .subscribe(
        data => {
          // console.log('token', data['response']);
          if (data['response'] == 'Email already exists!!') {
            this.errorMessage = 'Email already exists!!';
            this.showErrorMessage = true;
            // this.mat.close(false);
          } else {
            this.mat.close(result);
            this.config.openSnackBar('Customer Details Updated');
            this.showErrorMessage = false;
          }

          //console.log("data updated",data);
          //window.location.reload();
        },
        error => {
          //console.log(error);
        }
      );
  }




  workOrder(result) {
    this.updateCustomer(result);
    // localStorage.setItem('userData', null);
    // localStorage.setItem("userData", JSON.stringify(result));
    this.globalService.getCustomerData(result);

    this.router.navigate(['jobs']);
    this.mat.close();

  }

}

