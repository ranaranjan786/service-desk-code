import { Component, OnInit, Inject } from '@angular/core';
// import { FormControl } from '@angular/forms';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { fuseAnimations } from '../../../core/animations';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AuthServiceService } from '../../../../services/auth-service.service';
import { Router } from '@angular/router';
import { Config } from '../../../../config';
import { Validation } from '../../../../services/validation';
import { GlobalService } from '../../../../services/global-service';

@Component({
    selector: 'adddialog',
    templateUrl: './addCustomer.component.html',
    styleUrls: ['./addCustomer.component.scss'],
    animations: fuseAnimations
})
export class addCustomerComponent {

    showWorkOrder: boolean;
    constructor(
        private authService: AuthServiceService,
        private router: Router,
        private mat: MatDialogRef<addCustomerComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any,
        private config: Config,
        private validate: Validation,
        private globalService: GlobalService,


    ) {
        this.showWorkOrder = this.data['showWorkOrder']

    }
    customer: any = {};
    flags: any = {};
    errorMessage = '';


    workOrder(result) {


        this.addCustomer(result);
        this.router.navigate(['jobs']); //navigate to jobs component
        let url = this.router.url;
        if (url == "/jobs") {
            debugger;
        }
        this.globalService.getCustomerData(result);

        // this.mat.close(result);
        this.mat.close();

    }


    addCustomer(customer) {

        // return new Promise((resolve, reject) => {

        if (!this.validate.checkValidations(customer)) {
            return;
        }

        if (customer.altphone == undefined) {
            customer.altphone = '';
        }
        if (customer.address2 == undefined) {
            customer.address2 = '';
        }

        customer.company_id=JSON.parse(localStorage.getItem('company_id'));
        customer.status_id= 1, //Static
        customer.enteredbyuser_ID=JSON.parse(localStorage.getItem('user_id'));
        // let customerObj = {
        //     customer: customer,
        //     company_id: JSON.parse(localStorage.getItem('company_id')),
        //     status_id: 1, //Static
        //     enteredbyuser_ID: JSON.parse(localStorage.getItem('user_id'))
        // }
 
        this.authService.addCustomers(customer)
            .subscribe(
                data => {
                    // console.log("customer", data);
                    // console.log('token', data['response']);
                    if (data['response'] == 'Email already exists!!') {
                        this.errorMessage = 'Email already exists!!';
                    } else {
                        if (data['response'] == 'success') {
                            customer.id = data['customer_id'];
                            customer.customerId = data['customer_id'];
                            this.mat.close(customer);
                            this.config.openSnackBar('Customer Added Successfully');
                            // ev.stopPropagation();
                            // resolve(customer);
                        }
                    }
                },
                error => {
                    this.config.openSnackBar('Error! Customer Not Added');
                    // reject(error);
                }

            );
        // });

    }


}

