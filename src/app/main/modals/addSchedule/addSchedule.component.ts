import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { fuseAnimations } from '../../../core/animations';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
    selector     : 'adddialog',
    templateUrl: './addSchedule.component.html',
    styleUrls  : ['./addSchedule.component.scss'],
    animations : fuseAnimations
})
export class addScheduleComponent
{

}

