import { Component, OnInit, Inject } from '@angular/core';
import { FormControl } from '@angular/forms';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { setConfigFuseConfigService } from '../../../core/services/config.service';
import { fuseAnimations } from '../../../core/animations';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AuthServiceService } from '../../../../services/auth-service.service';
import { Router } from '@angular/router';
import { Config } from '../../../../config';
import { Validation } from '../../../../services/validation';

@Component({
    selector: 'editdialog',
    templateUrl: './addService.component.html',
    styleUrls: ['./addService.component.scss'],
    animations: fuseAnimations
})
export class addServiceComponent implements OnInit {
    addService: FormGroup;
    addServiceErrors: any;
    user: any = {};
    errorResponse = '';
    errorMessage = '';
    responseData = [];
    customer_id;
    showErrorMessage: boolean = false;
    constructor(
        private fuseConfig: setConfigFuseConfigService,
        private formBuilder: FormBuilder,
        private authService: AuthServiceService,
        private router: Router,
        private mat: MatDialogRef<addServiceComponent>,
        private config: Config,
        private validate: Validation,
        @Inject(MAT_DIALOG_DATA)
        public data: any,

    ) {

        this.addServiceErrors = {
            first_name: {},
            last_name: {},
            email: {},
            altphone: {},
            address2: {},
            phone: {},
            address1: {},
            city: {},
            state: {},
            zip: {},
            saletax: {}
        };
    }
    ngOnInit() {
        this.customer_id = this.data['customerID'];
        console.log("this.customer_id", this.customer_id);
        this.addService = this.formBuilder.group({
            first_name: ['', [Validators.required]],
            last_name: ['', [Validators.required]],
            email: ['', [Validators.required, Validators.email]],
            phone: ['', [Validators.required]],
            altphone: ['', []],
            address1: ['', [Validators.required]],
            address2: ['', []],
            city: ['', [Validators.required]],
            state: ['', [Validators.required]],
            zip: ['', [Validators.required]],
            saletax: ['', [Validators.required]]
        });
        // this.addService.valueChanges.subscribe(() => {
        //     this.onLoginFormValuesChanged();
        // });
    }
    // onLoginFormValuesChanged()
    // {
    //     for ( const field in this.addServiceErrors )
    //     {
    //         if ( !this.addServiceErrors.hasOwnProperty(field) )
    //         {
    //             continue;
    //         }

    //         // Clear previous errors
    //         this.addServiceErrors[field] = {};

    //         // Get the control
    //         const control = this.addService.get(field);

    //         if ( control && control.dirty && !control.valid )
    //         {
    //             this.addServiceErrors[field] = control.errors;
    //         }
    //     }
    // }

    submitfn(user) {
        if (!this.validate.checkValidations(user)) {
            return;
        }
        if (user.altphone == undefined) {
            user.altphone = '';
        }
        if (user.address2 == undefined) {
            user.address2 = '';
        }
        user.customer_id = this.customer_id;
        user.company_id = JSON.parse(localStorage.getItem('company_id'));
        user.status_id = 1; //static
        user.enteredbyuser_ID = JSON.parse(localStorage.getItem('user_id'));
        this.authService.addClientService(user)
            .subscribe(
                (data: any) => {
                    console.log('addservice', data);


                    if (data['response'] == 'Email already exists!!') {
                        this.errorMessage = 'Email already exists!!';
                        this.showErrorMessage = true;

                    } else {
                        this.config.openSnackBar('Client Added Successfully');
                        user.id = data.response;
                        this.responseData = user;
                        this.mat.close(this.responseData);
                        this.showErrorMessage = false;


                        // console.log("login response",data);
                        localStorage.setItem("addservice", data['response']);
                    }
                },
                error => {
                    console.log(error);
                }
            );

    }

}

