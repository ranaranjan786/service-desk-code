import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { fuseAnimations } from '../../../core/animations';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
    selector     : 'adddialog',
    templateUrl: './editSchedule.component.html',
    styleUrls  : ['./editSchedule.component.scss'],
    animations : fuseAnimations
})
export class editScheduleComponent
{
selectedtime1 = 'optiontime2';
selectedtime2 = 'optiontimemin3';
selectedtime3 = 'optiontimesec3';
selectedtime4 = 'optiontimesecd4';
appointmentname = 'appointoption1';
emailbefore = 'emailoption1';
}

