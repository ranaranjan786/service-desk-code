import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NavigationEnd, NavigationStart, Router } from '@angular/router';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { AuthServiceService } from '../../../services/auth-service.service';
import { setConfigFuseConfigService } from '../../core/services/config.service';
import { addCustomerComponent } from '../modals/addCustomer/addCustomer.component';
import { editCustomerComponent } from '../modals/editCustomer/editCustomer.component';



@Component({
    selector: 'fuse-toolbar',
    templateUrl: './toolbar.component.html',
    styleUrls: ['./toolbar.component.scss']
})

export class FuseToolbarComponent implements OnInit {

    showLoadingBar: boolean;
    horizontalNav: boolean;
    resData: any = [];
    searchBarValue: string;
    vm=undefined;
    query=undefined;
    search;

    constructor(
        private fuseConfig: setConfigFuseConfigService,
        private formBuilder: FormBuilder,
        private router: Router,
        private authService: AuthServiceService,
        private matDialog: MatDialog

    ) {


        router.events.subscribe(
            (event) => {
                if (event instanceof NavigationStart) {
                    this.showLoadingBar = true;
                }
                if (event instanceof NavigationEnd) {
                    this.showLoadingBar = false;
                }
            });


    }
    name = '';
    ngOnInit() {
        this.name = localStorage.getItem("user_name");

         

    }

    myProfile(){
        this.router.navigate(['my-profile']);
    }

    ngOnDestroy(){
        // var token = localStorage.getItem('token');
        // if (token != null) {
        //     var tokenVal = {
        //         tokenValue: token
        //     }
        //     this.authService.loginCheck(tokenVal)
        //         .subscribe(
        //             (data: any) => {

        //                 if (data.result != "error") {
        //                     this.router.navigate(['dashboard']);
        //                 }
        //                 else {
        //                     this.router.navigate(['login']);
        //                     localStorage.setItem('token', '');
        //                 }
        //                 //console.log("login response",data);
        //                 // console.log('token', data['response']);
        //             },
        //             error => {
        //                 console.log(error);
        //             }
        //         );
        // } else {
        //     this.router.navigate(['/']);
        // }
    }

    logoutFn() {

        var tokenVal = localStorage.getItem('token');
        var token = {
            tokenValue: tokenVal
        };
        this.authService.getLogout(token)
            .subscribe(
                data => {
                    //  console.log("login response",data);
                    if (data['response'] == 'success') {
                        localStorage.clear();
                        this.router.navigate(['/']);
                    }
                    else {
                        localStorage.clear();
                        this.router.navigate(['/']);
                    }
                },
                error => {
                    console.log(error);
                }
            );
    }

    addCustomerComponent() {
        this.matDialog.open(addCustomerComponent, {
            panelClass: 'editdialog',
            data: { showWorkOrder: true }

        });
    }

    // search(ev: any) {

    //     let val = ev.target.value;
    //     var customerNames = {
    //         name: val
    //     }

    //     this.authService.matchCustomerNames(customerNames).subscribe((res: any) => {
    //         this.resData = res.response;
    //     })
    // }

    clickFn(option) {
        // alert('id'+option.id)
        let id = option.id;
        // console.log('toolbarpageid', id);
        // localStorage.setItem('id', JSON.stringify(id));
        // this.resData=[];
        this.searchBarValue = '';
        this.matDialog.open(editCustomerComponent, {
            panelClass: 'editdialog',
            data: { showWorkOrder: true, id: id }

        });
    }

    //     myControl: FormControl = new FormControl();

    //   options = [
    //     'One',
    //     'Two',
    //     'Three'
    //   ];

    //   filteredOptions: Observable<string[]>;

    //   filter(val: string): string[] {
    //     return this.options.filter(option =>
    //       option.toLowerCase().indexOf(val.toLowerCase()) === 0);
    //   }
}
