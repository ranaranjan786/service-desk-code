import { Component } from '@angular/core';
import { FuseSplashScreenService } from './core/services/splash-screen.service';
import { TranslateService } from '@ngx-translate/core';
import { FuseTranslationLoaderService } from './core/services/translation-loader.service';

import { FuseNavigationService } from './core/components/navigation/navigation.service';
import { FuseNavigationModel } from './navigation/navigation.model';
import { locale as navigationEnglish } from './navigation/i18n/en';
import { locale as navigationTurkish } from './navigation/i18n/tr';
import { AuthServiceService } from '../services/auth-service.service';
import { Router } from '@angular/router';
import { GlobalService } from '../services/global-service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {
    constructor(
        private fuseNavigationService: FuseNavigationService,
        private fuseSplashScreen: FuseSplashScreenService,
        private translate: TranslateService,
        private translationLoader: FuseTranslationLoaderService,
        private authService: AuthServiceService,
        private router: Router,
        private globalService: GlobalService
    ) {
        // Add languages
        this.translate.addLangs(['en', 'tr']);

        // Set the default language
        this.translate.setDefaultLang('en');

        // Use a language
        this.translate.use('en');

        // Set the navigation model
        this.fuseNavigationService.setNavigationModel(new FuseNavigationModel());

        // Set the navigation translations
        this.translationLoader.loadTranslations(navigationEnglish, navigationTurkish);



        this.checkUrlComapnyName();


        var token = localStorage.getItem('token');
        if (token != null) {
            var tokenVal = {
                tokenValue: token
            }
            this.authService.loginCheck(tokenVal)
                .subscribe(
                    (data: any) => {

                        if (data.result != "error") {
                             this.router.navigate([this.router.url]);
                        }
                        else {
                            this.router.navigate(['login']);
                            localStorage.setItem('token', '');
                        }
                        //console.log("login response",data);
                        // console.log('token', data['response']);
                    },
                    error => {
                        console.log(error);
                    }
                );
        } else {
            this.router.navigate(['/']);
        }
    }

    checkUrlComapnyName() {
        let completeUrl = this.globalService.getCompanyName();
        let indexOfFirstDot = completeUrl.indexOf('.');
        let indexOfLastDot = completeUrl.lastIndexOf('.');
        if (indexOfFirstDot == indexOfLastDot) {
            this.router.navigate(['']);
        }
        else {
            let obj = {
                company_name: window.location.href
            }
            this.authService.checkCompany(obj)
                .subscribe((data: any) => {

                    if (data.response == "success") {
                        this.router.navigate(['']);
                    }
                    else {
                        // alert("Error 404");
                        this.router.navigate(['error']);
                    }
                },
            (err:any)=>{
                this.router.navigate(['error']);
            });
        }
        // let tempUrl = completeUrl.split('.')[0];
    }
}
