import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule, Routes } from '@angular/router';
import { InMemoryWebApiModule } from 'angular-in-memory-web-api';
import 'hammerjs';
import { SharedModule } from './core/modules/shared.module';
import { AppComponent } from './app.component';
import { DbService } from './db/db.service';
import { FuseMainModule } from './main/main.module';
import { FuseSplashScreenService } from './core/services/splash-screen.service';
import { setConfigFuseConfigService } from './core/services/config.service';
import { FuseNavigationService } from './core/components/navigation/navigation.service';
import { LoginModule } from './main/pages/authentication/login/login.module';
import { ForgotPasswordModule } from './main/pages/authentication/forgot-password/forgot-password.module';
import { HomeModule } from './main/pages/home/home.module';
import { JobsModule } from './main/pages/jobs/jobs.module';
import { EstimatesModule } from './main/pages/jobs/estimates.module';
import { InvoiceModule } from './main/pages/jobs/invoice.module';
import { PaymentsModule } from './main/pages/jobs/payments.module';
import { ContractsModule } from './main/pages/jobs/contracts.module';
import { ReportsModule } from './main/pages/reports/reports.module';
// import { CustomersModule } from './main/pages/customers/customers.module';
import { FuseTodoModule } from './main/pages/myJobs/myJobs.module';
import { CalendarPageModule } from './main/pages/calendar/calendar.module';
import { TranslateModule } from '@ngx-translate/core';
import { addCustomerComponent } from './main/modals/addCustomer/addCustomer.component';
import { editCustomerComponent } from './main/modals/editCustomer/editCustomer.component';
import { addServiceComponent } from './main/modals/addService/addService.component';
import { editServiceComponent } from './main/modals/editService/editService.component';
import { addScheduleComponent } from './main/modals/addSchedule/addSchedule.component';  
import { editScheduleComponent } from './main/modals/editSchedule/editSchedule.component';  
import { AuthServiceService } from '../services/auth-service.service';
// import { FuseLoginComponent } from './main/pages/authentication/login/login.component';
import { HomeComponent } from './main/pages/home/home.component';
import { FuseForgotPasswordComponent } from './main/pages/authentication/forgot-password/forgot-password.component';
import { JobsComponent } from './main/pages/jobs/jobs.component';
import { MyJobModule } from './main/pages/jobs/my-job.module';
import { Config } from '../config';
import { FuseToolbarComponent } from './main/toolbar/toolbar.component';
import { Validation } from '../services/validation';
import { UpdatePasswordModule } from './main/pages/authentication/update-password/update-password.module';
import { GlobalService } from '../services/global-service';
import { Error404Module } from './main/pages/errors/error-404.module';
// import {UpdatePasswordComponent} from './main/pages/authentication/update-password/update-password.component'
import { MyHttpInterceptor } from '../services/http-interceptor';
import { CustomersModule } from './main/pages/customers/customers.module';
import { CustomerDetailModule } from './main/pages/customers/customers-detail/customerDetail.module';
import { MyProfileModule } from './main/pages/myProfile/myProfile.module';



const appRoutes: Routes = [
    {
        path: 'toolbar',
        component: FuseToolbarComponent
    },
    // {
    //     path: '',
    //     component: FuseLoginComponent
    // },
    // {
    //     path: 'login',
    //     component: FuseLoginComponent
    // },
    {
        path: 'dashboard',
        component: HomeComponent
    },
    {
        path: 'forget-password',
        component: FuseForgotPasswordComponent
    },
    {
        path: 'jobs',
        component: JobsComponent
    },
    // {
    //     path: 'reset-password',
    //     component: UpdatePasswordComponent
    // }
];

// export const routing = RouterModule.forRoot(appRoutes, {
//     useHash: true
// });

@NgModule({
    declarations: [
        AppComponent,
        addCustomerComponent,
        editCustomerComponent,
        addServiceComponent,
        editServiceComponent,
        addScheduleComponent,
        editScheduleComponent,

    ],
    entryComponents: [
        addCustomerComponent,
        editCustomerComponent,
        addServiceComponent,
        editServiceComponent,
        addScheduleComponent,
        editScheduleComponent,

    ],
    imports: [
        BrowserModule,
        HttpClientModule,
        BrowserAnimationsModule,
        RouterModule.forRoot(appRoutes, {
            useHash: true
        }),
        SharedModule,
        TranslateModule.forRoot(),
        InMemoryWebApiModule.forRoot(DbService, {
            delay: 0,
            passThruUnknownUrl: true
        }),
        FuseMainModule,

        // Login page module  
        LoginModule,

        // Forgot Pass page module  
        ForgotPasswordModule,

        // Home module
        HomeModule,

        // Jobs-Jobs module
        JobsModule,

        // Jobs-Estimates module
        EstimatesModule,

        // Jobs-Invoice module
        InvoiceModule,

        // Jobs-Payments module
        PaymentsModule,

        // Jobs-Contracts module
        ContractsModule,

        // Reports module
        ReportsModule,

        FuseTodoModule,

        // Customers module
        // CustomersModule,

        // Calendar module
        CalendarPageModule,
        MyJobModule,
        // UpdatePasswordModule,
        Error404Module,
        CustomersModule,
        CustomerDetailModule,
        MyProfileModule







        // addCustomer module



    ],
    providers: [
        FuseSplashScreenService,
        setConfigFuseConfigService,
        FuseNavigationService,
        AuthServiceService,
        Config,
        GlobalService,
        Validation,
        { 
            provide: HTTP_INTERCEPTORS, 
            useClass: MyHttpInterceptor, 
            multi: true 
        } 

    ],
    bootstrap: [
        AppComponent
    ]
})
export class AppModule {
}
