import { Injectable } from '@angular/core';
import { MatSnackBarConfig, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition, MatSnackBar } from '@angular/material';


@Injectable()
export class Config {
    // url = "http://192.168.1.151/helpdesk/laravelAng/public/"; //localPRSir
    // url='http://helpdesk.com/helpdesk/' //localShaileshSir


    url = 'http://1servicecloud.com/helpdesk/'; //live
    // companyUrl="http://1servicecloud.com/helpdesk/"; //local
    companyUrl=window.location.origin + window.location.pathname; //live
    // urlSplitString = 'http://1servicecloud.com';

    public horizontalPosition: MatSnackBarHorizontalPosition = 'right';
    public verticalPosition: MatSnackBarVerticalPosition = 'top';
    public setAutoHide: boolean = true;
    public autoHide: number = 2000;
    public addExtraClass: boolean = false;


    constructor(private snackBar: MatSnackBar, ) {

    }

    public snackBarConfig() {
        var config = new MatSnackBarConfig();
        config.verticalPosition = this.verticalPosition;
        config.horizontalPosition = this.horizontalPosition;
        config.duration = this.setAutoHide ? this.autoHide : 0;
        config.panelClass = this.addExtraClass ? ['party'] : undefined;
        return config;
    }

    openSnackBar(message: string) {

        this.snackBar.open(message, undefined, this.snackBarConfig());
    }
}