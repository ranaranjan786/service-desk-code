import { Injectable } from '@angular/core';
import { Config } from '../config';
@Injectable()

export class Validation {
    regExFname:RegExp=/^[a-zA-Z\s]+$/;
    regExLname: RegExp = /^[a-zA-Z]*$/;
    regExEmail: RegExp = /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/;
    regExPhone: RegExp = /^\d{10}$/;
    constructor(private config: Config) { }

    checkValidations(obj) {


        return (
            this.fName(obj.first_name) &&
            this.lName(obj.last_name) &&
            this.email(obj.email) &&
            this.phone(obj.phone) &&
            this.phone(obj.altphone) &&
            this.zip(obj.zip)
        )
    }


    fName(name:string){
        if (this.regExFname.test(name)) {
            return true;
        } else {
            this.config.openSnackBar('Please Enter a Valid Name');
            return false;
        };
    }

    lName(name: string) {
        if (this.regExLname.test(name)) {
            return true;
        } else {
            this.config.openSnackBar('Please Enter a Valid Name');
            return false;
        };
    }

    email(email: string) {
        if (this.regExEmail.test(email)) {
            return true;
        }
        else {
            this.config.openSnackBar('Please Enter a Valid Email');
            return false;
        };
    }

    phone(phone: any) {
        if (phone == undefined || phone == '' || this.regExPhone.test(phone)) {
            return true;
        }
        else {
            this.config.openSnackBar('Please Enter a Valid Phone Number');
            return false;
        };
    }

    zip(zip) {                  //only 6 chars allowed
        if (zip.length == 6) {
            return true;
        }
        else {
            this.config.openSnackBar('Please Enter a Valid Zip');
            return false;
        };
    }

}