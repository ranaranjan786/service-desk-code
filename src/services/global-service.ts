import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Config } from '../config';
import { Router, NavigationEnd } from '@angular/router';
// import { AuthServiceService } from './auth-service.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';


@Injectable()
export class GlobalService {

    private customerData = new BehaviorSubject<any>({});
    currentCustomerData = this.customerData.asObservable();
    private ticketID = new BehaviorSubject<any>(0);
    currentTicketID = this.customerData.asObservable();
    private customerDetailedData = new BehaviorSubject<any>({})
    currentCustomerDetailedData = this.customerDetailedData.asObservable();

    companyName = "";

    constructor(private config: Config,
        private router: Router,
        // private authService: AuthServiceService,
        private http: HttpClient,
    ) {
    }

    getCustomerData(data: any) {
        if (data) {
            this.customerData.next(data);
            //    console.log("EsyRtrndata", data);
            // this.returnData = data;
        }
    }

    getCurrentCustomerDetailedData(data) {
        if (data) {
            this.customerDetailedData.next(data);
        }
    }

    getTicketId(id: number) {
        if (id) {
            this.customerData.next(id);
            //    console.log("EsyRtrndata", data);
            // this.returnData = data;
        }
    }

    getCompanyName() {

        // let str = window.location.href.split(this.config.urlSplitString)[0];
        // let str1 = str.substr(str.indexOf('//') + 2);
        // this.companyName = str1;
        // return str1;
        this.companyName = window.location.origin;
        return this.companyName;
    }

    checkCompanyName() {
        if (localStorage.getItem('company_name')) {
            // let oldCompanyName = localStorage.getItem('company_name').split('4501')[0]; //local
            // let newCompanyName = this.getCompanyName().split('4501')[0]; //local
            let oldCompanyName = localStorage.getItem('company_name').split('.')[0]; //live
            let newCompanyName = this.getCompanyName().split('.')[0] //live

            if (oldCompanyName == newCompanyName) {

                return true;
            }
            else {
                var tokenVal = localStorage.getItem('token');
                var token = {
                    tokenValue: tokenVal
                };
                let url = this.config.url + "logout";
                return this.http.post(url, token, {
                    headers: new HttpHeaders({
                        'Content-Type': 'application/json',

                    })
                })
                    .subscribe(
                        data => {
                            //  console.log("login response",data);
                            if (data['response'] == 'success') {
                                localStorage.clear();
                                this.router.navigate(['/']);
                            }
                            else {
                                localStorage.clear();
                                this.router.navigate(['/']);
                            }
                        },
                        error => {
                            console.log(error);
                        }
                    );
                // this.router.navigate[''];
                // this.toolbar.logoutFn();


            }
        }

    }

    checkIfJobUrl() {
        // console.log("this.router.url",this.router.url);
        // if (this.router.url != 'jobs') {
        //     localStorage.setItem('ticketId', null);
        //     localStorage.setItem('jobsPageData', null);
        // }

        this.router.events.subscribe((val) => {
            // see also 
            console.log(val instanceof NavigationEnd)
        });

    }


}