import { Injectable, Injector } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/observable/throw'
import 'rxjs/add/operator/catch';
import { Config } from '../config';


@Injectable()
export class MyHttpInterceptor implements HttpInterceptor {
    constructor(private config:Config) { }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        // console.log("intercepted request ... ");

        // Clone the request to add the new header.
        const authReq = req.clone({
            setHeaders: {
                'Access-Token': this.getToken(),
                'Content-Type': 'application/json'
            }
        });

        // console.log("Sending request with new header now ...");

        //send the newly created request
        return next.handle(authReq)
            .catch((error, caught) => {
                //intercept the respons error and displace it to the console
                // console.log("Error Occurred");
                console.log(error);
                this.config.openSnackBar('Error Occured');
                //return the error to the method that called it
                return Observable.throw(error);
            }) as any;
    }

    getToken() {
        //   this.verifyToken();
        if (localStorage.getItem('token') != null)
            return localStorage.getItem('token');
        else
            return "null";
    }
}