import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpClientXsrfModule } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Config } from '../config';
import { GlobalService } from './global-service';
import { Router } from '@angular/router';
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    // "Access-Control-Allow-Headers": "Origin, X-Requested-With, Content-Type, Accept",
    // "Access-Control-Allow-Origin": "*",
  })
};


@Injectable()
export class AuthServiceService {
  // url= 'http://127.0.0.1:8000/';

  constructor(private http: HttpClient,
    private config: Config,
    private globalService: GlobalService,
    private router: Router) {

    this.globalService.checkCompanyName();

    // if (this.router.url != "/jobs") { debugger
    //   localStorage.setItem('ticketId', null);
    //   localStorage.setItem('jobsData', null);
    // }
    // this.globalService.checkIfJobUrl();
  }

  getLogin(user) {
    // user.name = "abc";
    let URL = this.config.url + "login";
    // let head = {
    //   headers: new HttpHeaders({
    //     'Content-Type': 'application/json'
    //   })
    // }
    return this.http.post(URL, user, httpOptions);

  }

  getLogout(token) {
    let URL = this.config.url + "logout";
    return this.http.post(URL, token, httpOptions);
    //return this.http.get(URL,httpOptions);
  }

  loginCheck(token) {
    let URL = this.config.url + "getAuthUser";
    return this.http.post(URL, token, httpOptions);
  }

  addClientService(user) {
    let URL = this.config.url + "saveClientService";
    return this.http.post(URL, user, httpOptions);
  }
  editClientService(user) {
    let URL = this.config.url + "editClientService";
    return this.http.post(URL, user, httpOptions);
  }
  getClientdata(id) {
    let URL = this.config.url + "getClientData";
    return this.http.post(URL, id, httpOptions);
  }
  getClientService(user) {
    let URL = this.config.url + "ClientService";
    return this.http.post(URL, user, httpOptions);
  }
  getClientServiceData(service) {
    let URL = this.config.url + "getClientServiceData";
    return this.http.post(URL, service, httpOptions);
  }

  /**
   * To add new customers 
   */
  addCustomers(customer) {

    //let URL = "http://127.0.0.1:8000/getCustomers";
    return this.http.post(this.config.url + 'getCustomers', customer, httpOptions);
  }

  /**
   * to get all cuastomer names for job page
   * 
  */
  getAllCustomer(user) {
    
    //let URL = "http://127.0.0.1:8000/getCustomerNames";
    return this.http.post(this.config.url + 'getCustomerNames', user, httpOptions);

  }
  /**
   * to get data on change customer name
   */
  getCustomerdata(customerid) {
    //let URL = "http://127.0.0.1:8000/getCustomerDetails";
    return this.http.post(this.config.url + 'getCustomerDetails', customerid, httpOptions);


  }

  /**
   * to edit customer details
   */
  editCustomerdata(customerid) {
    //let URL = "http://127.0.0.1:8000/editCustomerDetails";
    return this.http.post(this.config.url + 'editCustomerDetails', customerid, httpOptions);
  }
  /**
   * to update customer details
   */
  updateCustomerdata(result) {
    return this.http.post(this.config.url + 'updateCustomerDetails', result, httpOptions);
  }

  matchCustomerNames(customerNames) {
    //console.log('names',customerNames);
    return this.http.post(this.config.url + 'getCustomerNamesByCharacter', customerNames, httpOptions);
  }

  getAllUserData(company_id) {
    return this.http.post(this.config.url + 'getUserDetailsByCmpId', company_id, httpOptions);

  }

  createWorkOrder(jobs_data) {
    return this.http.post(this.config.url + 'insertjobs', jobs_data, httpOptions);
  }

  updateWorkOrder(work_data) {
    return this.http.post(this.config.url + 'update_workorder', work_data, httpOptions);
  }

  forgetPassword(user_email) {
    return this.http.post(this.config.url + 'forgetPassword', user_email, httpOptions);
  }

  resetPassword(password) {
    return this.http.post(this.config.url + 'resetPassword', password, httpOptions);
  }

  validateToken(test_token) {
    return this.http.post(this.config.url + 'validateToken', test_token, httpOptions);
  }

  getJobData(id) {
    return this.http.post(this.config.url + 'editjobs', id, httpOptions);
  }

  updateJobData(job) {
    return this.http.post(this.config.url + 'updatejobs', job, httpOptions);
  }

  getAllWorkOrder(userId){
    return this.http.post(this.config.url + 'getAllOrder', userId, httpOptions);
  }

  checkCompany(companyUrl){
    return this.http.post(this.config.url + 'checkCompany', companyUrl, httpOptions);
  }

  customerServiceDetails(object){
    return this.http.post(this.config.url + 'customerServiceDetails', object, httpOptions);
  }

  deleteCustomer(customer){
    return this.http.post(this.config.url + 'deleteCustomer', customer, httpOptions);
  }

  userDetails(user){
    return this.http.post(this.config.url + 'userDetails', user, httpOptions);
  }

  updateUserDetails(user){
    return this.http.post(this.config.url + 'updateUserDetails', user, httpOptions);
  }

  getCustomerServiceUpdate(customer){
    return this.http.post(this.config.url + 'getCustomerServiceUpdate', customer, httpOptions);
  }

}



